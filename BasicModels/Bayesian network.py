from pomegranate import *
from sklearn.model_selection import train_test_split
from Helper.FormatAndObtainData import returnXYData
import pandas as pd

X,y,le,noOfClasses = returnXYData()

# Split dataset into training set and test set
X_train, X_test, y_train, y_test = train_test_split(X, y, test_size=0.3)

X = pd.concat([X_train,pd.DataFrame(y_train)], axis=1)
#model = BayesianNetwork.from_samples(X50LagAccOnly, algorithm='exact')
model = BayesianNetwork.from_samples(X)

X_test["DE"] = "None"
print(model.predict(X_test))