from sklearn import metrics
from sklearn.metrics import confusion_matrix, precision_score
from sklearn.model_selection import train_test_split
from sklearn.neural_network import MLPClassifier

from Helper.FormatAndObtainData import *

#X,y,le,noOfClasses = returnXYData()
X= pd.read_pickle("../InputPickleFiles/50LagAccOnly/X50LagAccOnly")
y= pd.read_pickle("y50lag")

# Split dataset into training set and test set
X_train, X_test, y_train, y_test = train_test_split(X, y, test_size=0.3)

clf = MLPClassifier(random_state=1, max_iter=300).fit(X_train, y_train)
y_pred = clf.predict(X_test)


print("MLP")

print("Number of layers = ",clf.n_layers_)
print("Encoded Data")
"""
for i in range(noOfClasses):
    print(i , "=", le.inverse_transform([i]))

print()"""
print("Accuracy:",metrics.accuracy_score(y_test, y_pred))

print("Precision Score = " , precision_score(y_test, y_pred,average="weighted"))


#fpr, tpr, thresholds = metrics.roc_curve(y_test, y_pred,pos_label=)
#print(metrics.auc(fpr, tpr))

print("Confusion Matrix:  x-axis = Predicated Label, y-axis = True Label")
print(confusion_matrix(y_test, y_pred))