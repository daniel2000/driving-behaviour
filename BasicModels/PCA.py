import datetime

from sklearn.decomposition import PCA
from sklearn.preprocessing import StandardScaler
from tensorboard.plugins.hparams import api as hp

from Helper.HelperClass import *
import tensorflow as tf

from Helper.FormatAndObtainData import *


X= pd.read_pickle("../InputPickleFiles/50LagAllData/X50LagAllData")
y= pd.read_pickle("../InputPickleFiles/50LagAllData/y50lagAllData")



"""
pca = PCA(.95)
for index, row in X.iterrows():
    if row.isnull().values.any():
        print("dsad")
    #for indexCol, col in row.iteritems():


print(X.isnull().values.any())

pca.fit(X)
train_img = pca.transform(X)

print("No of Variables " , pca.n_components_)
"""


X_train, X_test, y_train, y_test = train_test_split(X, y, test_size=0.3)

scaler = StandardScaler()
# Fit on training set only.
scaler.fit(X_train)
# Apply transform to both the training set and the test set.
train_img = scaler.transform(X_train)
test_img = scaler.transform(X_test)


# Make an instance of the Model
pca = PCA(.95)

pca.fit(train_img)

train_img = pca.transform(train_img)
test_img = pca.transform(test_img)


#X_train = train_img.to_numpy()
#y_train = y_train.to_numpy()
#X_test = test_img.to_numpy()
#y_test = y_test.to_numpy()

X_train = np.reshape(train_img, (train_img.shape[0], train_img.shape[1], 1))
X_test = np.reshape(test_img, (test_img.shape[0], test_img.shape[1], 1))

epochs = 1000
noOfNeuronsInOutputLayer = 7
dateAndTimeNow = datetime.datetime.now().strftime("%Y%m%d-%H%M%S")
step = X_train.shape[1]


HP_NUM_UNITS = hp.HParam('num_units', hp.Discrete([50]))
HP_BATCH_SIZE = hp.HParam('batch_size', hp.Discrete([20]))
HP_ACT_FUNC = hp.HParam('activation_function', hp.Discrete(["sigmoid"]))
HP_OPTIMIZER = hp.HParam('optimizer', hp.Discrete(["rmsprop"]))

METRIC_ACCURACY = 'accuracy'

with tf.summary.create_file_writer('logs/hparam_tuning/' + dateAndTimeNow).as_default():
    hp.hparams_config(
        hparams=[HP_NUM_UNITS, HP_BATCH_SIZE,HP_ACT_FUNC],
        metrics=[hp.Metric(METRIC_ACCURACY, display_name='Accuracy')],
    )


def train_test_model(hparams):
    model = Sequential()
    #(step,1)
    model.add(Dense(input_shape=(step,1), units=hparams[HP_NUM_UNITS], activation=hparams[HP_ACT_FUNC]))
    model.add(GRU(units=hparams[HP_NUM_UNITS], activation=hparams[HP_ACT_FUNC]))
    model.add(Dense(noOfNeuronsInOutputLayer, activation=hparams[HP_ACT_FUNC]))
    model.compile(loss='mean_squared_error', optimizer=hparams[HP_OPTIMIZER], metrics=['acc'])


    model.fit(X_train, y_train, epochs=epochs,batch_size=hparams[HP_BATCH_SIZE],callbacks=[tf.keras.callbacks.TensorBoard("logs/fit/" + dateAndTimeNow)]) # Run with 1 epoch to speed things up for demo purposes
    _, accuracy = model.evaluate(X_test, y_test)

    return accuracy

def run(run_dir, hparams):
  with tf.summary.create_file_writer(run_dir).as_default():
    hp.hparams(hparams)  # record the values used in this trial
    accuracy = train_test_model(hparams)
    tf.summary.scalar(METRIC_ACCURACY, accuracy, step=1)



session_num = 0

for num_units in HP_NUM_UNITS.domain.values:
    for batch_size in HP_BATCH_SIZE.domain.values:
        for act_func in HP_ACT_FUNC.domain.values:
            for optimizer in HP_OPTIMIZER.domain.values:
              hparams = {
                  HP_NUM_UNITS: num_units,
                  HP_BATCH_SIZE: batch_size,
                  HP_ACT_FUNC:act_func,
                  HP_OPTIMIZER:optimizer,
              }
              run_name = "run-%d" % session_num
              print('--- Starting trial: %s' % run_name)
              print({h.name: hparams[h] for h in hparams})
              run('logs/hparam_tuning/' + dateAndTimeNow + run_name, hparams)
              session_num += 1
