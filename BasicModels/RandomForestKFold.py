from sklearn import metrics
from sklearn.ensemble import RandomForestClassifier
from sklearn.metrics import multilabel_confusion_matrix, precision_score, accuracy_score, f1_score
from sklearn.model_selection import train_test_split, StratifiedKFold
from Helper.FormatAndObtainData import *
from sklearn.model_selection import GridSearchCV
from os import path


import numpy as np

def convertToIndividualArray(X_train):
    temp = np.empty((X_train.shape[0], 50, 3))
    for rowNo, row in enumerate(X_train):
        for colNo in range(0, len(row), 3):
            temp2 = int(colNo / 3)
            temp[rowNo, temp2, 0] = X_train[rowNo, colNo]
            temp[rowNo, temp2, 1] = X_train[rowNo, colNo + 1]
            temp[rowNo, temp2, 2] = X_train[rowNo, colNo + 2]

    return temp

def revert_onehot_encoding(input_array):
    list =  [np.where(r==1)[0][0] for r in input_array]
    return np.array(list)


def convert_to_one_hot_encoding(array):
    output_array = np.zeros((len(array),7))
    for counter,item in enumerate(array):
        output_array[counter][item] = 1

    return output_array


# https://datascience.stackexchange.com/questions/24108/multiple-time-series-predictions-with-random-forests-in-python
# https://towardsdatascience.com/random-forest-in-python-24d0893d51c0

#X50LagAccOnly,y50lagAccOnly = returnXYData(50,5)
lag = 10
if path.exists("X" + str(lag - 1) + "LinearAccData") == True:
    X_orig = pd.read_pickle("X" + str(lag - 1) + "LinearAccData")
    y_orig = pd.read_pickle("y" + str(lag - 1) + "LinearAccData")
else:
    # Obtaining the csv file having all outputs on 1 coloumn
    X_orig, y_orig = returnXYData(lag - 1, 5)

    X_orig.to_pickle("X" + str(lag - 1) + "LinearAccData")
    y_orig.to_pickle("y" + str(lag - 1) + "LinearAccData")
X= pd.read_pickle("../InputPickleFiles/49LagLinearAccData/X49LagLinearAccData")
y= pd.read_pickle("../InputPickleFiles/49LagLinearAccData/y49lagLinearAccData")
X = X.to_numpy()
y = y.to_numpy()

skf = StratifiedKFold(n_splits=5)

y = revert_onehot_encoding(y)

X_train, X_test, y_train, y_test = train_test_split( X, y, test_size=0.2)
parameters = {'n_estimators':[100,500,1000],'max_features':["auto","sqrt","log2"]}
random_forest=RandomForestClassifier()

clf = GridSearchCV(random_forest,parameters,n_jobs=-1,cv=5,scoring=['accuracy','f1_weighted'],refit='f1_weighted',verbose=3)
clf.fit(X_train,y_train)

y_pred = clf.predict(X_test)

print("Acc:" + str(accuracy_score(y_test,y_pred)))
print("F1 Score Weighted:" + str(f1_score(y_test,y_pred,average='weighted')))

print(clf.cv_results_.keys())
for key in clf.cv_results_.keys():
    print(str(key) + ":" + str(clf.cv_results_[key]))


"""
print(clf.cv_results_['param_n_estimators'])
print(clf.cv_results_['params'])
print(clf.cv_results_['split0_test_score'])
print(clf.cv_results_['split1_test_score'])
print(clf.cv_results_['split2_test_score'])
print(clf.cv_results_['split3_test_score'])
print(clf.cv_results_['split4_test_score'])
print("Mean Test Score")
print(clf.cv_results_['mean_test_score'])
print(clf.cv_results_['std_test_score'])
print(clf.cv_results_['rank_test_score'])
"""