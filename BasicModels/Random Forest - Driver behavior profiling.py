from sklearn import metrics
from sklearn.ensemble import RandomForestClassifier
from sklearn.metrics import multilabel_confusion_matrix, precision_score
from sklearn.model_selection import train_test_split
from Helper.FormatAndObtainData import *

import numpy as np

def convertToIndividualArray(X_train):
    temp = np.empty((X_train.shape[0], 50, 3))
    for rowNo, row in enumerate(X_train):
        for colNo in range(0, len(row), 3):
            temp2 = int(colNo / 3)
            temp[rowNo, temp2, 0] = X_train[rowNo, colNo]
            temp[rowNo, temp2, 1] = X_train[rowNo, colNo + 1]
            temp[rowNo, temp2, 2] = X_train[rowNo, colNo + 2]

    return temp

# https://datascience.stackexchange.com/questions/24108/multiple-time-series-predictions-with-random-forests-in-python
# https://towardsdatascience.com/random-forest-in-python-24d0893d51c0

#X50LagAccOnly,y50lagAccOnly = returnXYData(50,5)
X= pd.read_pickle("../InputPickleFiles/49LagLinearAccData/X49LagLinearAccData")
y= pd.read_pickle("../InputPickleFiles/49LagLinearAccData/y49lagLinearAccData")
X = X.to_numpy()
y = y.to_numpy()
#X = convertToIndividualArray(X)
# Split dataset into training set and test set
X_train, X_test, y_train, y_test = train_test_split(X, y, test_size=0.2)


clf=RandomForestClassifier(n_estimators=1000)

#Train the model using the training sets y_pred=clf.predict(X_test)
clf.fit(X_train,y_train)

y_pred=clf.predict(X_test)
print("Random Forest ")
print("Accuracy:",metrics.accuracy_score(y_test, y_pred))

print("Precision Score = " , precision_score(y_test, y_pred,average="micro"))

print("Encoded Data")

#fpr, tpr, thresholds = metrics.roc_curve(y_test, y_pred,pos_label=)
#print(metrics.auc(fpr, tpr))

print(multilabel_confusion_matrix(y_test,y_pred))

#fpr, tpr, thresholds = metrics.roc_curve(y_test, y_pred, pos_label=2)
#print("AUC: ", metrics.auc(fpr, tpr))


