import linecache
import os

import numpy as np
import pandas as pd
import tensorflow as tf
from sklearn import metrics
from sklearn.model_selection import train_test_split
from collections import Counter
from sklearn.metrics import multilabel_confusion_matrix, precision_score, confusion_matrix, recall_score, f1_score, \
    plot_confusion_matrix


def getFileLineCount(fname):
    with open(fname) as f:
        for i, l in enumerate(f):
            pass
    return i + 1

def getDictValue(dict,eventType):
    if dict.get(eventType) is None:
        dict[eventType] = [0,0]
        return dict[eventType]
    else:
        return dict.get(eventType)


def getclassName(label):
    if label == 0:
        return("Non Aggressive Event")
    elif label == 1:
        return("Aggressive Right Turn")
    elif label == 2:
        return("Aggressive Left Turn")
    elif label == 3:
        return("Aggressive Acceleration")
    elif label == 4:
        return("Aggressive Braking")
    elif label == 5:
        return("Aggressive Left Lane Change")
    elif label == 6:
        return("Aggressive Right Lane Change")


true = []
pred = []
# Load the TFLite model and allocate tensors.
#interpreter = tf.lite.Interpreter(model_path="ModelFiles/ExactCopy4-Acc0.9974014759063721.tflite")
# 50 Lag with my data
#interpreter = tf.lite.Interpreter(model_path="May 2021/UAH13Lag100EpochModel.tflite")

#Sliding Window 50 With Acceleration Data from Driver Behaviour Dataset
interpreter = tf.lite.Interpreter(model_path="May 2021/50AccData3000.9993506669998169.tflite")

#24 Lag with my data
#interpreter = tf.lite.Interpreter(model_path="ModelFiles/ExactCopy4-Acc0.9478390216827393.tflite")

interpreter.allocate_tensors()

# Get input and output tensors.
input_details = interpreter.get_input_details()
output_details = interpreter.get_output_details()

# Test the model on random input data.
input_shape = input_details[0]['shape']

lag=50

outputCounter = 0
fileLineCounter = 0
percentageCorrectList = []

classAverage = {}

dirName = [5,7,10,11]
y_test_global = []
y_pred_global = []



for dir in dirName:
    rootdir = "C:\\Users\\attar\\Desktop\\FYP\\Data\\Data Collection "+ str(dir)+"\\Accelerometer Arrays"
    #rootdir = "C:\\Users\\attar\\Desktop\\AirSim Runs\\Output Arrays\\54"

    for subdir, dirs, files in os.walk(rootdir):

        exampleFile = None
        xFile = None
        yFile = None
        zFile = None
        eventType = -1

        for file in files:
            if file[0] == "X":
                xFile = open(os.path.join(subdir, file))
                exampleFile = os.path.join(subdir, file)
            elif file[0] == "Y":
                yFile = open(os.path.join(subdir, file))
            elif file[0] == "Z":
                zFile = open(os.path.join(subdir, file))
            elif file == "EventType.txt":
                eventTypeFile = open(os.path.join(subdir, file))
                eventType = int(eventTypeFile.readline())
            else:
                raise FileNotFoundError

        if xFile is None or yFile is None or zFile is None or eventType == -1:
            continue

        array = np.empty((1, lag, 3), dtype=np.float32)



        xLines = xFile.readlines()
        yLines = yFile.readlines()
        zLines = zFile.readlines()

        fileCount = getFileLineCount(exampleFile)
        outputDataArr = np.zeros((fileCount-(lag-1),7),dtype=np.float32)
        outputCounter = 0
        counter = lag -1


        while(True):
            if fileLineCounter == fileCount:
                fileLineCounter = 0
                break

            array[0][counter][0] = xLines[fileLineCounter].rstrip()
            array[0][counter][1] = yLines[fileLineCounter].rstrip()
            array[0][counter][2] = zLines[fileLineCounter].rstrip()
            counter -= 1
            fileLineCounter +=1

            if counter == -1:
                fileLineCounter = fileLineCounter - (lag-1)
                interpreter.set_tensor(input_details[0]['index'], array)

                interpreter.invoke()

                # The function `get_tensor()` returns a copy of the tensor data.
                # Use `tensor()` in order to get a pointer to the tensor.
                output_data = interpreter.get_tensor(output_details[0]['index'])
                outputDataArr[outputCounter] = output_data
                outputCounter += 1
                counter = lag -1
                y_test_global.append(eventType)

        y_pred_global.extend(np.argmax(outputDataArr,axis = 1))


        test_predictions = np.round(outputDataArr)
        sums = np.sum(test_predictions, axis=1)

        numberOfInsignificantEvents = 0

        for sum in sums:
            if sum == 0:
                numberOfInsignificantEvents = numberOfInsignificantEvents + 1
        predictions = np.argmax(test_predictions, axis=1)
        print("Directory" ,subdir)
        print("Actual = ", eventType)
        print("Insignificant Events = ",numberOfInsignificantEvents)
        print(Counter(predictions).keys())
        print(Counter(predictions).values())

        true.append(eventType)
        pred.append(Counter(predictions).most_common(1)[0][0])
        total = 0

        for value in Counter(predictions).values():
            total += value

        numberOfCorrectCases = Counter(predictions).get(eventType)
        if numberOfCorrectCases is None:
            numberOfCorrectCases = 0

        percentageCorrect = round(( numberOfCorrectCases/ total ) *100,2)
        print("Percentage Correct:" , percentageCorrect , "%")
        percentageCorrectList.append(percentageCorrect)
        print()
        print()

        classAverage[eventType] = [getDictValue(classAverage,eventType)[0] + percentageCorrect, getDictValue(classAverage,eventType)[1] +1]


    #print("Average percentage = ", np.average(percentageCorrectList))

for classKey in classAverage:
    print("Average for class " + str(classKey) +" (" +getclassName(classKey)+") = " + str(round(classAverage.get(classKey)[0] / classAverage.get(classKey)[1],2)) + "% correct predictions")

print("X Axis Predicted Class")
print("Y Axis Actual Class")
print(confusion_matrix(true,pred))
print("Precision:", precision_score(true,pred,average="macro"))
print("Recall:", recall_score(true,pred,average="macro"))




def convertToIndividualArray(X_train):
    X, Y = [], []
    temp = np.empty((X_train.shape[0],1, lag, 3),dtype=np.float32)
    for rowNo, row in enumerate(X_train):
        for colNo in range(0, len(row), 3):
            temp2 = int(colNo / 3)
            temp[rowNo,0, temp2, 0] = X_train[rowNo, colNo]
            temp[rowNo,0, temp2, 1] = X_train[rowNo, colNo + 1]
            temp[rowNo,0, temp2, 2] = X_train[rowNo, colNo + 2]

    return temp

print("--------------------------------------")
print("--------------------------------------")
print("--------------------------------------")
print("--------------------------------------")

conf_matrix = confusion_matrix(y_test_global,y_pred_global)
print(conf_matrix)
print("F1 Score" + str(f1_score(y_test_global,y_pred_global,average="weighted")))
"""

y_test = np.argmax(y_test,axis=1)
predictions = np.argmax(outputDataArr,axis=1)
print(metrics.multilabel_confusion_matrix(y_test, predictions))

max = -1

for output in output_data[0]:
    if output >max:
        max = output

finalArray = []
for output in output_data[0]:
    if output == max:
        finalArray.append(1)
    else:
        finalArray.append(0)

print(finalArray)
"""
