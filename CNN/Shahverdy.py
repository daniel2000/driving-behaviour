# https://www.datatechnotes.com/2018/12/rnn-example-with-keras-simplernn-in.html
import datetime

from keras.layers import LSTM
from sklearn.model_selection import train_test_split
from tensorflow.python.keras.utils.vis_utils import plot_model
from tensorboard.plugins.hparams import api as hp

from Helper.HelperClass import *
import tensorflow as tf

from Helper.FormatAndObtainData import *
from keras.layers import Dense, Concatenate, Input,Conv1D,LeakyReLU,MaxPooling1D,Dropout,GlobalMaxPooling1D
from keras.models import Model
from keras import Sequential
from keras import losses,optimizers
from pyts.image import RecurrencePlot
import cv2


from Helper.ModelConverterToTFLITE import convertModelToTFLITE

"""
Proposed RNN topology. 
The inputs take into account THREE VECTORS of 50 values each. 
There is ONE HIDDEN layer with RECURRENCE (GRU) (with 2, 5 and 10 neurons). 

The output is mapped as a multi-label problem; 
each output neuron is linked to an event type, as shown in Table I.
"""



# convert into dataset matrix
def convertToMatrix(data, step):
    X, Y = [], []
    for i in range(len(data) - step):
        d = i + step
        X.append(data[i:d, ])
        Y.append(data[d,])
    return np.array(X), np.array(Y)


def convertToIndividualArray(X_train):
    temp = np.empty((X_train.shape[0], 50, 3))
    for rowNo, row in enumerate(X_train):
        for colNo in range(0, len(row), 3):
            temp2 = int(colNo / 3)
            temp[rowNo, temp2, 0] = X_train[rowNo, colNo]
            temp[rowNo, temp2, 1] = X_train[rowNo, colNo + 1]
            temp[rowNo, temp2, 2] = X_train[rowNo, colNo + 2]

    return temp


N = 1000
Tp = 800
epochs = 400
noOfNeuronsInOutputLayer = 7
dateAndTimeNow = datetime.datetime.now().strftime("%Y%m%d-%H%M%S")

# Obtaining the csv file having all outputs on 1 coloumn
# X49LagAccData,y49lagAccData = returnXYData(49, 5)

# X49LagAccData.to_pickle("X49LagAccData")
# y49lagAccData.to_pickle("y49lagAccData")

X = pd.read_pickle("../InputPickleFiles/49LagAccOnly/X49LagAccData")
y = pd.read_pickle("../InputPickleFiles/49LagAccOnly/y49lagAccData")

X_train, X_test, y_train, y_test = train_test_split(X, y, test_size=0.3)

X_train = X_train.to_numpy()
y_train = y_train.to_numpy()
X_test = X_test.to_numpy()
y_test = y_test.to_numpy()

#X_train = np.reshape(X_train, (X_train.shape[0], X_train.shape[1], 1))
#X_test = np.reshape(X_test, (X_test.shape[0], X_test.shape[1], 1))
X_train = convertToIndividualArray(X_train)
X_test = convertToIndividualArray(X_test)


step = X_train.shape[1]

HP_FILTER_SIZE = hp.HParam('filter_size', hp.Discrete([2,3,5,7]))
HP_FILTER_COUNT = hp.HParam('filter_count', hp.Discrete([16,32]))

METRIC_ACCURACY = 'accuracy'


transformer = RecurrencePlot()

x = X_train[0][0]

image = transformer.transform(x)

cv2.imshow("Test",image)
cv2.waitKey(0)

with tf.summary.create_file_writer('logs/hparam_tuning/' + dateAndTimeNow).as_default():
    hp.hparams_config(
        hparams=[HP_FILTER_SIZE, HP_FILTER_COUNT],
        metrics=[hp.Metric(METRIC_ACCURACY, display_name='Accuracy')],
    )


def train_test_model2(hparams):
    model = Sequential()
    model.add(Conv1D(hparams[HP_FILTER_COUNT],(hparams[HP_FILTER_SIZE])))
    model.add(LeakyReLU(0.2))
    model.add(Conv1D(2*hparams[HP_FILTER_COUNT],(hparams[HP_FILTER_SIZE])))
    model.add(LeakyReLU(0.2))
    model.add(MaxPooling1D((2)))
    model.add(Dropout(0.25))

    model.add(GlobalMaxPooling1D())
    model.add(Dense(30,activation='relu'))
    model.add(Dense(7,activation='softmax'))

    model.compile(loss=losses.categorical_crossentropy, optimizer=optimizers.Adadelta(), metrics=['acc'])
    plot_model(model, to_file='model_plot' + dateAndTimeNow + '.png', show_shapes=True, show_layer_names=True)

    model.fit(X_train, y_train, epochs=epochs, batch_size=32, callbacks=[tf.keras.callbacks.TensorBoard("logs/fit/" + dateAndTimeNow)])  # Run with 1 epoch to speed things up for demo purposes
    test_predictions = model.predict(X_test)
    _, accuracy = model.evaluate(X_test, y_test)

    convertModelToTFLITE(model, "Shahverdy-Acc" + str(accuracy))
    return accuracy


"""
def train_test_model(hparams):
    model = Sequential()
    # model.add(Dense(input_shape=(step, 1), units=hparams[HP_NUM_UNITS], activation=hparams[HP_ACT_FUNC]))
    model.add(GRU(input_shape=(step, 1), return_sequences=True, units=3, activation="softmax"))
    model.add(GRU(10, activation="softmax"))
    model.add(Dense(hparams[HP_NUM_UNITS], activation="sigmoid"))
    model.compile(loss='mean_squared_error', optimizer=hparams[HP_OPTIMIZER], metrics=['acc'])
    plot_model(model, to_file='model_plot' + dateAndTimeNow + '.png', show_shapes=True, show_layer_names=True)

    model.fit(X_train, y_train, epochs=epochs, batch_size=hparams[HP_BATCH_SIZE], callbacks=[
        tf.keras.callbacks.TensorBoard(
            "logs/fit/" + dateAndTimeNow)])  # Run with 1 epoch to speed things up for demo purposes
    _, accuracy = model.evaluate(X_test, y_test)

    return accuracy
"""

def run(run_dir, hparams):
    with tf.summary.create_file_writer(run_dir).as_default():
        hp.hparams(hparams)  # record the values used in this trial
        accuracy = train_test_model2(hparams)
        tf.summary.scalar(METRIC_ACCURACY, accuracy, step=1)


session_num = 0


for filter_size in HP_FILTER_SIZE.domain.values:
    for filter_count in HP_FILTER_COUNT.domain.values:
                hparams = {
                    HP_FILTER_SIZE: filter_size,
                    HP_FILTER_COUNT: filter_count,
                }
                run_name = "run-%d" % session_num
                print('--- Starting trial: %s' % run_name)
                print({h.name: hparams[h] for h in hparams})
                run('logs/hparam_tuning/' + dateAndTimeNow + run_name, hparams)
                session_num += 1
