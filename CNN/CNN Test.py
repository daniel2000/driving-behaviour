# https://www.datatechnotes.com/2018/12/rnn-example-with-keras-simplernn-in.html
import datetime

from Helper.ModelConverterToTFLITE import *

from keras.models import Sequential
from keras.layers import Dense, Flatten, Conv1D
from sklearn.model_selection import train_test_split
from tensorflow.python.keras.utils.vis_utils import plot_model
from tensorboard.plugins.hparams import api as hp

from Helper.HelperClass import *
import tensorflow as tf

from Helper.FormatAndObtainData import *


# convert into dataset matrix
def convertToMatrix(data, step):
    X, Y = [], []
    for i in range(len(data) - step):
        d = i + step
        X.append(data[i:d, ])
        Y.append(data[d,])
    return np.array(X), np.array(Y)

def convertToIndividualArray(X_train):
    temp = np.empty((X_train.shape[0], 50, 3))
    for rowNo, row in enumerate(X_train):
        for colNo in range(0, len(row), 3):
            temp2 = int(colNo / 3)
            temp[rowNo, temp2,0] = X_train[rowNo, colNo]
            temp[rowNo, temp2,1] = X_train[rowNo, colNo + 1]
            temp[rowNo, temp2,2] = X_train[rowNo, colNo + 2]

    return temp

N = 1000
Tp = 800
epochs = 1000
noOfNeuronsInOutputLayer = 7
dateAndTimeNow = datetime.datetime.now().strftime("%Y%m%d-%H%M%S")

X= pd.read_pickle("../InputPickleFiles/49LagAccOnly/X49LagAccData")
y= pd.read_pickle("../InputPickleFiles/49LagAccOnly/y49lagAccData")


X_train, X_test, y_train, y_test = train_test_split(X, y, test_size=0.3)

X_train = X_train.to_numpy()
y_train = y_train.to_numpy()
X_test = X_test.to_numpy()
y_test = y_test.to_numpy()

#X_train = np.reshape(X_train, (X_train.shape[0], X_train.shape[1], 1))
#X_test = np.reshape(X_test, (X_test.shape[0], X_test.shape[1], 1))
X_train = convertToIndividualArray(X_train)
X_test = convertToIndividualArray(X_test)
step = X_train.shape[1]

# Batch 1
HP_NUM_UNITS = hp.HParam('num_units', hp.Discrete([50]))
HP_BATCH_SIZE = hp.HParam('batch_size', hp.Discrete([20]))

# Batch 2
#HP_NUM_UNITS = hp.HParam('num_units', hp.Discrete([7]))
#HP_BATCH_SIZE = hp.HParam('batch_size', hp.Discrete([32]))

HP_ACT_FUNC = hp.HParam('activation_function', hp.Discrete(["softmax"]))
HP_OPTIMIZER = hp.HParam('optimizer', hp.Discrete(["rmsprop"]))
HP_KERNEL_SIZE = hp.HParam('kernal_size',hp.Discrete([1,3,5,7,9]))
HP_PADDING = hp.HParam('padding',hp.Discrete(["valid", "same" ,"causal"]))

METRIC_ACCURACY = 'accuracy'

with tf.summary.create_file_writer('logs/hparam_tuning/' + dateAndTimeNow).as_default():
    hp.hparams_config(
        hparams=[HP_NUM_UNITS, HP_BATCH_SIZE,HP_ACT_FUNC,HP_OPTIMIZER,HP_KERNEL_SIZE,HP_PADDING],
        metrics=[hp.Metric(METRIC_ACCURACY, display_name='Accuracy')],
    )


def train_test_model(hparams):
    model = Sequential()
    model.add(Conv1D(input_shape=(step, 3),filters=hparams[HP_NUM_UNITS], kernel_size=hparams[HP_KERNEL_SIZE], activation=hparams[HP_ACT_FUNC],padding=hparams[HP_PADDING]))
    model.add(Flatten())
    model.add(Dense(noOfNeuronsInOutputLayer, activation=hparams[HP_ACT_FUNC]))
    plot_model(model, to_file='model_plot' + dateAndTimeNow + '.png', show_shapes=True, show_layer_names=True)

    model.compile(loss='mean_squared_error', optimizer=hparams[HP_OPTIMIZER], metrics=['acc'])
    model.summary()


    model.fit(X_train, y_train, epochs=epochs,batch_size=hparams[HP_BATCH_SIZE],callbacks=[tf.keras.callbacks.TensorBoard("logs/fit/" + dateAndTimeNow)]) # Run with 1 epoch to speed things up for demo purposes
    _, accuracy = model.evaluate(X_test, y_test)

    convertModelToTFLITE(model,"CNN-Acc" + str(accuracy))

    return accuracy

def run(run_dir, hparams):
  with tf.summary.create_file_writer(run_dir).as_default():
    hp.hparams(hparams)  # record the values used in this trial
    accuracy = train_test_model(hparams)
    tf.summary.scalar(METRIC_ACCURACY, accuracy, step=1)



session_num = 0

for num_units in HP_NUM_UNITS.domain.values:
    for batch_size in HP_BATCH_SIZE.domain.values:
        for act_func in HP_ACT_FUNC.domain.values:
            for optimizer in HP_OPTIMIZER.domain.values:
                for kernel_size in HP_KERNEL_SIZE.domain.values:
                    for padding in HP_PADDING.domain.values:
                        hparams = {
                          HP_NUM_UNITS: num_units,
                          HP_BATCH_SIZE: batch_size,
                          HP_ACT_FUNC:act_func,
                          HP_OPTIMIZER:optimizer,
                          HP_KERNEL_SIZE:kernel_size,
                          HP_PADDING:padding
                        }
                        run_name = "run-%d" % session_num
                        print('--- Starting trial: %s' % run_name)
                        print({h.name: hparams[h] for h in hparams})
                        run('logs/hparam_tuning/' + dateAndTimeNow + run_name, hparams)
                        session_num += 1
