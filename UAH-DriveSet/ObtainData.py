import os
import csv
# importing panda library
import pandas as pd

def table2lags(table, max_lag, min_lag=0, separator='_'):
    """ Given a dataframe, return a dataframe with different lags of all its columns """
    values=[]
    for i in range(min_lag, max_lag + 1):
        values.append(table.shift(i).copy())
        values[-1].columns = [c + separator + str(i) for c in table.columns]
    return pd.concat(values, axis=1)

def obtain_data_lag(lag):
    eventDataframes = []
    main_dir = "C:\\Users\\attar\\Desktop\\FYP\\Datasets\\UAH-DRIVESET-v1"

    testcounter = 0
    for subdir, dirs, files in os.walk(main_dir):
        #print(subdir)
        #print(dirs)
        #print(files)
        head_tail = os.path.split(subdir)

        try:
            head_tail[1].split('-')[3]
        except IndexError:
            continue
        tempcounter = 0



        if head_tail[1].split('-')[3] == "AGGRESSIVE" or head_tail[1].split('-')[3] == "NORMAL"  or head_tail[1].split('-')[3] == "NORMAL1":


            for events_file_no in range(2):

                if (head_tail[1].split('-')[3] == "NORMAL" or head_tail[1].split('-')[3] == "NORMAL1") and events_file_no == 1:
                    break

                print("Processing event file " + str(events_file_no) + " of " + str(head_tail[1].split('-')[2]) + " "+ str(head_tail[1].split('-')[3]))

                linearAccDataDir = os.path.join(subdir, "RAW_ACCELEROMETERS.csv")
                laneChangeEventsDir = os.path.join(subdir, "EVENTS_LIST_LANE_CHANGES.csv")
                truthEventDir = os.path.join(subdir, "EVENTS_INERTIAL.csv")

                linearAccData = pd.read_csv(linearAccDataDir)
                lanceChangeEvents = pd.read_csv(laneChangeEventsDir)
                truthEvents = pd.read_csv(truthEventDir)

                events_files = [truthEvents,lanceChangeEvents]

                eventDataframes.append(pd.DataFrame())

                counter = 0

                subcounter = 0
                for index, dataRow in linearAccData.iterrows():
                    if counter == 500:
                        # break
                        pass

                    if counter != 0:
                        for indexTruthRow, truthRow in events_files[events_file_no].iterrows():
                            eventEncoded = False
                            if events_file_no == 0 and float(truthRow[6]) <= float(dataRow[0]) <= float(truthRow[7]) or \
                                    events_file_no == 1 and float(truthRow[0]) <= float(dataRow[0]) <= float(truthRow[0]) + float(truthRow[5]):

                                #linearAccData.at[counter, "event"] = truthRow[1]
                                eventDataframes[len(eventDataframes) - 1].at[subcounter, "Timestamp"] = dataRow[0]
                                eventDataframes[len(eventDataframes) - 1].at[subcounter, "x"] = dataRow[2] * 9.80665
                                eventDataframes[len(eventDataframes) - 1].at[subcounter, "y"] = dataRow[3] * 9.80665
                                eventDataframes[len(eventDataframes) - 1].at[subcounter, "z"] = dataRow[4] * 9.80665

                                if events_file_no == 0:
                                    if truthRow[1] == -1:
                                        eventDataframes[len(eventDataframes) - 1].at[subcounter, "nonAggressive"] = 1
                                        eventDataframes[len(eventDataframes) - 1].at[
                                            subcounter, "aggressiveRightTurn"] = 0
                                        eventDataframes[len(eventDataframes) - 1].at[
                                            subcounter, "aggresiveLeftTurn"] = 0
                                        eventDataframes[len(eventDataframes) - 1].at[
                                            subcounter, "aggressiveAcceleration"] = 0
                                        eventDataframes[len(eventDataframes) - 1].at[
                                            subcounter, "aggressiveBraking"] = 0
                                        eventDataframes[len(eventDataframes) - 1].at[
                                            subcounter, "aggressiveLeftLaneChange"] = 0
                                        eventDataframes[len(eventDataframes) - 1].at[
                                            subcounter, "aggressiveRightLaneChange"] = 0
                                    elif truthRow[1] == 1:
                                        eventDataframes[len(eventDataframes) - 1].at[subcounter, "nonAggressive"] = 0
                                        eventDataframes[len(eventDataframes) - 1].at[
                                            subcounter, "aggressiveRightTurn"] = 0
                                        eventDataframes[len(eventDataframes) - 1].at[
                                            subcounter, "aggresiveLeftTurn"] = 0
                                        eventDataframes[len(eventDataframes) - 1].at[
                                            subcounter, "aggressiveAcceleration"] = 0
                                        eventDataframes[len(eventDataframes) - 1].at[
                                            subcounter, "aggressiveBraking"] = 1
                                        eventDataframes[len(eventDataframes) - 1].at[
                                            subcounter, "aggressiveLeftLaneChange"] = 0
                                        eventDataframes[len(eventDataframes) - 1].at[
                                            subcounter, "aggressiveRightLaneChange"] = 0
                                    elif truthRow[1] == 2 and truthRow[8] == 1:
                                        eventDataframes[len(eventDataframes) - 1].at[subcounter, "nonAggressive"] = 0
                                        eventDataframes[len(eventDataframes) - 1].at[
                                            subcounter, "aggressiveRightTurn"] = 1
                                        eventDataframes[len(eventDataframes) - 1].at[
                                            subcounter, "aggresiveLeftTurn"] = 0
                                        eventDataframes[len(eventDataframes) - 1].at[
                                            subcounter, "aggressiveAcceleration"] = 0
                                        eventDataframes[len(eventDataframes) - 1].at[
                                            subcounter, "aggressiveBraking"] = 0
                                        eventDataframes[len(eventDataframes) - 1].at[
                                            subcounter, "aggressiveLeftLaneChange"] = 0
                                        eventDataframes[len(eventDataframes) - 1].at[
                                            subcounter, "aggressiveRightLaneChange"] = 0
                                    elif truthRow[1] == 2 and truthRow[8] == -1:
                                        eventDataframes[len(eventDataframes) - 1].at[subcounter, "nonAggressive"] = 0
                                        eventDataframes[len(eventDataframes) - 1].at[
                                            subcounter, "aggressiveRightTurn"] = 0
                                        eventDataframes[len(eventDataframes) - 1].at[
                                            subcounter, "aggresiveLeftTurn"] = 1
                                        eventDataframes[len(eventDataframes) - 1].at[
                                            subcounter, "aggressiveAcceleration"] = 0
                                        eventDataframes[len(eventDataframes) - 1].at[
                                            subcounter, "aggressiveBraking"] = 0
                                        eventDataframes[len(eventDataframes) - 1].at[
                                            subcounter, "aggressiveLeftLaneChange"] = 0
                                        eventDataframes[len(eventDataframes) - 1].at[
                                            subcounter, "aggressiveRightLaneChange"] = 0
                                    elif truthRow[1] == 3:
                                        eventDataframes[len(eventDataframes) - 1].at[subcounter, "nonAggressive"] = 0
                                        eventDataframes[len(eventDataframes) - 1].at[
                                            subcounter, "aggressiveRightTurn"] = 0
                                        eventDataframes[len(eventDataframes) - 1].at[
                                            subcounter, "aggresiveLeftTurn"] = 0
                                        eventDataframes[len(eventDataframes) - 1].at[
                                            subcounter, "aggressiveAcceleration"] = 1
                                        eventDataframes[len(eventDataframes) - 1].at[
                                            subcounter, "aggressiveBraking"] = 0
                                        eventDataframes[len(eventDataframes) - 1].at[
                                            subcounter, "aggressiveLeftLaneChange"] = 0
                                        eventDataframes[len(eventDataframes) - 1].at[
                                            subcounter, "aggressiveRightLaneChange"] = 0
                                    else:
                                        raise ("Event not found")

                                    subcounter += 1
                                    eventEncoded = True
                                    break

                                if events_file_no == 1:
                                    if truthRow[1] == 1:
                                        eventDataframes[len(eventDataframes) - 1].at[
                                            subcounter, "nonAggressive"] = 0
                                        eventDataframes[len(eventDataframes) - 1].at[
                                            subcounter, "aggressiveRightTurn"] = 0
                                        eventDataframes[len(eventDataframes) - 1].at[
                                            subcounter, "aggresiveLeftTurn"] = 0
                                        eventDataframes[len(eventDataframes) - 1].at[
                                            subcounter, "aggressiveAcceleration"] = 0
                                        eventDataframes[len(eventDataframes) - 1].at[
                                            subcounter, "aggressiveBraking"] = 0
                                        eventDataframes[len(eventDataframes) - 1].at[
                                            subcounter, "aggressiveLeftLaneChange"] = 0
                                        eventDataframes[len(eventDataframes) - 1].at[
                                            subcounter, "aggressiveRightLaneChange"] = 1
                                    elif truthRow[1] == -1:
                                        eventDataframes[len(eventDataframes) - 1].at[
                                            subcounter, "nonAggressive"] = 0
                                        eventDataframes[len(eventDataframes) - 1].at[
                                            subcounter, "aggressiveRightTurn"] = 0
                                        eventDataframes[len(eventDataframes) - 1].at[
                                            subcounter, "aggresiveLeftTurn"] = 0
                                        eventDataframes[len(eventDataframes) - 1].at[
                                            subcounter, "aggressiveAcceleration"] = 0
                                        eventDataframes[len(eventDataframes) - 1].at[
                                            subcounter, "aggressiveBraking"] = 0
                                        eventDataframes[len(eventDataframes) - 1].at[
                                            subcounter, "aggressiveLeftLaneChange"] = 1
                                        eventDataframes[len(eventDataframes) - 1].at[
                                            subcounter, "aggressiveRightLaneChange"] = 0
                                    else:
                                        raise ("Event not found")

                                    subcounter += 1
                                    eventEncoded = True
                                    break

                        if not eventEncoded:
                            subcounter = 0
                            if not eventDataframes[len(eventDataframes) - 1].empty:
                                eventDataframes.append(pd.DataFrame())



                    counter += 1
                # Removing the last empty dataframe
                eventDataframes.pop()

    outputX = pd.DataFrame()
    outputY = pd.DataFrame()

    length_of_dataframe = []

    for dataframe in eventDataframes:
        length_of_dataframe.append([len(dataframe),list(dataframe.iloc[0,4:])])
        # Adding lag
        x = table2lags(dataframe[['x', 'y', 'z']], lag)
        outputX = outputX.append(x[lag:])
        tempDataFrame = dataframe[lag:]
        outputY = outputY.append(tempDataFrame[['nonAggressive', 'aggressiveRightTurn',
                                                'aggresiveLeftTurn', 'aggressiveAcceleration',
                                                'aggressiveBraking', 'aggressiveLeftLaneChange',
                                                'aggressiveRightLaneChange']])

    return outputX, outputY,length_of_dataframe

lag = 9
x,y,lengths = obtain_data_lag(lag)
print(x,y)

pd.to_pickle(x, "X_UAH_"+str(lag))
pd.to_pickle(y, "y_UAH_"+str(lag))