import pandas as pd
import csv
import math

def convert_seconds_to_nanoseconds(seconds):
    return seconds * 1000000000


def reduce_frequency(dataframe, new_frequency):
    total_datapoints = dataframe.shape[0]
    new_frequency_dataframe = pd.DataFrame()

    datapoints_per_group = total_datapoints/new_frequency

    if datapoints_per_group > 10.4:
        raise Exception("Inaccurate Divisions")

    datapoints_per_group = math.floor(datapoints_per_group)

    starting_index = 0
    stop_index = starting_index + datapoints_per_group

    for i in range(new_frequency):
        temp = dataframe[starting_index:stop_index].mean()
        temp = pd.Series(dataframe.iloc[0][0],index=['timestamp'],name="dad").append(temp)
        temp.reindex(dataframe.columns)


        new_frequency_dataframe= new_frequency_dataframe.append(temp,ignore_index=True)
        starting_index = starting_index + datapoints_per_group
        stop_index = stop_index + datapoints_per_group

        if i == new_frequency -2:
            stop_index = total_datapoints

    return new_frequency_dataframe












eventDataframes = []
# eventDataframes.append(pd.DataFrame())

driverNo = [16, 17, 20, 21]
startTimes = [11537640270059.0, 12893233616460.0, 10196945530109.0, 11200345835195.0]
new_second_start = [11538034074424.0,12894038957525.0,10197711591455,11200709912718]
main_dir = "C:\\Users\\attar\\Desktop\\FYP\\Datasets\\driverBehaviorDataset-masterLatest\\driverBehaviorDataset-master\\data\\"

for driverNoIndex in range(4):
    print("Starting player: " + str(driverNo[driverNoIndex]))
    accDataDir = main_dir + str(driverNo[driverNoIndex]) + "\\acelerometro_terra.csv"
    linearAccDataDir = main_dir + str(driverNo[driverNoIndex]) + "\\aceleracaoLinear_terra.csv"
    magneticFieldDataDir = main_dir + str(driverNo[driverNoIndex]) + "\\campoMagnetico_terra.csv"
    gyroscopeDataDir = main_dir +  str(driverNo[driverNoIndex]) + "\\giroscopio_terra.csv"
    truthEventDir = main_dir + str(driverNo[driverNoIndex]) + "\\groundTruth.csv"


    accData = pd.read_csv(accDataDir)
    linearAccData = pd.read_csv(linearAccDataDir)
    magneticFieldData = pd.read_csv(magneticFieldDataDir)
    gyroscopeData = pd.read_csv(gyroscopeDataDir)
    truthEvents = pd.read_csv(truthEventDir)


    # Read each row of the input csv file as list
    startTime = startTimes[driverNoIndex]

    previous_time = new_second_start[driverNoIndex]


    output_dataframe = pd.DataFrame()
    temp_dataframe = pd.DataFrame()
    for index, dataRow in accData.iterrows():
        if dataRow[1] < new_second_start[driverNoIndex]:
            continue

        if index == 0:
            output_dataframe = output_dataframe.append(dataRow)

        if dataRow[1] >= previous_time + convert_seconds_to_nanoseconds(1):
            output_dataframe = output_dataframe.append(reduce_frequency(temp_dataframe, 10))
            previous_time = dataRow[1]
            temp_dataframe.drop(temp_dataframe.index, inplace=True)
            temp_dataframe = temp_dataframe.append(dataRow)

        else:
            temp_dataframe = temp_dataframe.append(dataRow)

    output_dataframe.to_csv(main_dir + str(driverNo[driverNoIndex]) + "\\acelerometro_terra10Hz.csv",index=False)





