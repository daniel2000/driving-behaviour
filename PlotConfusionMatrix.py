from mlxtend.plotting import plot_confusion_matrix
import matplotlib.pyplot as plt
import numpy as np

class_names = ["Non-aggressive", "Aggressive Right Turn", "Aggressive Left Turn", "Aggressive Acceleration",
               "Aggressive Braking", "Aggressive Left Lane Change", "Aggressive Right Lane Change"]

conf_matrix = np.array([[161,2,0,4,4,4,5],[0,120,0,0,3,0,0],[0,0,82,0,0,0,0],[1,1,0,334,3,1,4],[3,1,0,5,270,1,3],[0,4,4,5,1,94,4],[6,1,2,8,8,0,100]])

fig, ax = plot_confusion_matrix(conf_mat=conf_matrix,
                                colorbar=False,
                                show_absolute=False,
                                show_normed=True)


plt.title("Confusion Matrix")
plt.tight_layout()
plt.savefig("conf2.png")
plt.show()
