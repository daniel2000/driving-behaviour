import datetime

from keras.layers import GRU
from sklearn.model_selection import train_test_split
from tensorflow.python.keras.utils.vis_utils import plot_model

from Helper.HelperClass import *

from Helper.FormatAndObtainData import *

from keras.layers import Dense, Concatenate, Input, Lambda
from keras.models import Model

dateAndTimeNow = datetime.datetime.now().strftime("%Y%m%d-%H%M%S")

def convertToIndividualArray(X_train):
    temp = np.empty((X_train.shape[0], 3, 50, 1))
    for rowNo, row in enumerate(X_train):
        for colNo in range(0, len(row), 3):
            temp2 = int(colNo / 3)
            temp[rowNo, 0, temp2] = X_train[rowNo, colNo]
            temp[rowNo, 1, temp2] = X_train[rowNo, colNo + 1]
            temp[rowNo, 2, temp2] = X_train[rowNo, colNo + 2]

    return temp


num_channels = 20

X= pd.read_pickle("../InputPickleFiles/49LagAccOnly/X49LagAccData")
y= pd.read_pickle("../InputPickleFiles/49LagAccOnly/y49lagAccData")

X_train, X_test, y_train, y_test = train_test_split(X, y, test_size=0.3)

X_train = X_train.to_numpy()
y_train = y_train.to_numpy()
X_test = X_test.to_numpy()
y_test = y_test.to_numpy()

X_train = convertToIndividualArray(X_train)
X_test = convertToIndividualArray(X_test)



#X_train = np.reshape(X_train, (X_train.shape[0], X_train.shape[1], 1))
#X_test = np.reshape(X_test, (X_test.shape[0], X_test.shape[1], 1))

input = Input(shape=(3, 50,1))

branch_outputs = []
for i in range(3):
    # Slicing the ith channel:
    out = Lambda(lambda x: x[:, i,:])(input)

    # Setting up your per-channel layers (replace with actual sub-models):
    out = GRU(units=50,activation="sigmoid")(out)
    branch_outputs.append(out)

# Concatenating together the per-channel results:
out = Concatenate()(branch_outputs)

# Adding some further layers (replace or remove with your architecture):
out = Dense(units=7)(out)

# Building model:
model = Model(inputs=input, outputs=out)
model.compile(loss='mean_squared_error', optimizer="rmsprop", metrics=['acc'])
plot_model(model, to_file='model_plot' + dateAndTimeNow + '.png', show_shapes=True, show_layer_names=True)

model.fit(X_train, y_train, epochs=2, batch_size=32)

