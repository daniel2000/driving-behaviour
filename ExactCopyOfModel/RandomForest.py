from sklearn import metrics
from sklearn.ensemble import RandomForestClassifier
from sklearn.metrics import multilabel_confusion_matrix, precision_score, accuracy_score, f1_score
from sklearn.model_selection import train_test_split, StratifiedKFold
from sklearn.preprocessing import LabelBinarizer
from tensorflow.python.keras.utils.np_utils import to_categorical

from Helper.FormatAndObtainData import *
from sklearn.model_selection import GridSearchCV
from os import path


import numpy as np

def convertToIndividualArray(X_train):
    temp = np.empty((X_train.shape[0], 50, 3))
    for rowNo, row in enumerate(X_train):
        for colNo in range(0, len(row), 3):
            temp2 = int(colNo / 3)
            temp[rowNo, temp2, 0] = X_train[rowNo, colNo]
            temp[rowNo, temp2, 1] = X_train[rowNo, colNo + 1]
            temp[rowNo, temp2, 2] = X_train[rowNo, colNo + 2]

    return temp

def revert_onehot_encoding(input_array):
    list =  [np.where(r==1)[0][0] for r in input_array]
    return np.array(list)


def convert_to_one_hot_encoding(array):
    output_array = np.zeros((len(array),7))
    for counter,item in enumerate(array):
        output_array[counter][item] = 1

    return output_array


# https://datascience.stackexchange.com/questions/24108/multiple-time-series-predictions-with-random-forests-in-python
# https://towardsdatascience.com/random-forest-in-python-24d0893d51c0

#X50LagAccOnly,y50lagAccOnly = returnXYData(50,5)+
#lag = 50,25,13
lag = 13
X = pd.read_pickle("X12AccData")
y = pd.read_pickle("y12AccData")
"""
if path.exists("X" + str(lag - 1) + "LinearAccData") == True:
    X = pd.read_pickle("X" + str(lag - 1) + "LinearAccData")
    y = pd.read_pickle("y" + str(lag - 1) + "LinearAccData")
else:
    # Obtaining the csv file having all outputs on 1 coloumn
    X, y = returnXYData(lag - 1, 5)

    X.to_pickle("X" + str(lag - 1) + "LinearAccData")
    y.to_pickle("y" + str(lag - 1) + "LinearAccData")
"""
X = X.to_numpy()
y = y.to_numpy()


for row in y:
    found = False
    for col in row:
        if col == 1:
            if found == False:
                found = True
            else:
                raise Exception("Egh")
    if found == False:
        raise Exception ("Error")

y= revert_onehot_encoding(y)
lb2 = LabelBinarizer()
lb2.fit(y)
print(lb2.y_type_)
#y = lb2.transform(y)



#skf_5fold_shuffled = StratifiedKFold(n_splits=5,shuffle=True)
X_train, X_test, y_train, y_test = train_test_split( X, y, test_size=0.2)

random_forest=RandomForestClassifier(n_estimators = 10,max_features="log2",max_depth=4)

random_forest.fit(X_train, y_train)
y_pred = random_forest.predict(X_test)

total_0 = 0
total_2 = 0
"""
for row in y_pred:
    total = sum(row)
    if total == 1:
        continue
    if total == 0:
        total_0+=1
    elif total == 2:
        total_2+=1

    else:
        print(total)
            """
for value in y_pred:
    if 0 <= value <= 6:
        continue
    else:
        raise Exception("dasds")
print("----------")
print(total_2)
print(total_0)
print("----------")
print("Acc:" + str(accuracy_score(y_test,y_pred)))
print("F1 Score Weighted:" + str(f1_score(y_test,y_pred,average='weighted')))

acc = []
f1 = []

estimator = random_forest.estimators_[5]
from sklearn.tree import export_graphviz
# Export as dot file
export_graphviz(estimator, out_file='tree.dot',
                feature_names = ["X0","Y0","Z0","X1","Y1","Z1","X2","Y2","Z2","X3","Y3","Z3","X4","Y4","Z4","X5","Y5","Z5","X6","Y6","Z6","X7","Y7","Z7","X8","Y8","Z8","X9","Y9","Z9","X10","Y10","Z10","X11","Y11","Z11","X12","Y12","Z12"],
                class_names = ["0","1","2","3","4","5","6"],
                rounded = True, proportion = False,
                precision = 2, filled = True)

# Convert to png using system command (requires Graphviz)
from subprocess import call
call(['dot', '-Tpng', 'tree.dot', '-o', 'tree.png', '-Gdpi=600'])

# Display in jupyter notebook
from IPython.display import Image
Image(filename = 'tree.png')

"""
for train_index, test_index in skf_5fold_shuffled.split(X, y):
    print("TRAIN:", train_index, "TEST:", test_index)
    X_train, X_test = X[train_index], X[test_index]
    y_train, y_test = y[train_index], y[test_index]
    #lb = LabelBinarizer()


    y_train = lb2.transform(y_train)
    y_test = lb2.transform(y_test)
    #y_train = to_categorical(y_train)
    #y_test = to_categorical(y_test)

    random_forest.fit(X_train, y_train)
    y_pred = random_forest.predict(X_test)

    print("Acc:" + str(accuracy_score(y_test,y_pred)))
    print("F1 Score Weighted:" + str(f1_score(y_test,y_pred,average='weighted')))

    acc.append(accuracy_score(y_test,y_pred))
    f1.append(f1_score(y_test,y_pred,average='weighted'))

print("Average Acc" + str(np.mean(acc)))
print("Average F1" + str(np.mean(f1)))
"""

