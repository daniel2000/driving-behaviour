# https://www.datatechnotes.com/2018/12/rnn-example-with-keras-simplernn-in.html
import datetime
import os
from os import path

from keras.callbacks import LambdaCallback
from sklearn.metrics import confusion_matrix
from sklearn.model_selection import train_test_split
from tensorboard.plugins.hparams import api as hp
from tensorflow.python.keras.utils.vis_utils import plot_model

from April2021.NewVersions.HelperFile import *
from Helper.FormatAndObtainData import *
from Helper.ModelConverterToTFLITE import convertModelToTFLITE
from April2021.TensorBoardHelperFiles import *

"""
Proposed RNN topology. 
The inputs take into account THREE VECTORS of 50 values each. 
There is ONE HIDDEN layer with RECURRENCE (GRU) (with 2, 5 and 10 neurons). 

The output is mapped as a multi-label problem; 
each output neuron is linked to an event type, as shown in Table I.
"""
testModel = None

noOfNeuronsInOutputLayer = 7
dateAndTimeNow = datetime.datetime.now().strftime("%Y%m%d-%H%M%S")



logdir = "logs/image/" + dateAndTimeNow
# Define the basic TensorBoard callback.
tensorboard_callback = tf.keras.callbacks.TensorBoard(log_dir=logdir)
file_writer_cm = tf.summary.create_file_writer(logdir + '/cm')


HP_NUM_UNITS = hp.HParam('num_units', hp.Discrete([7]))
HP_BATCH_SIZE = hp.HParam('batch_size', hp.Discrete([32]))
HP_ACT_FUNC = hp.HParam('activation_function', hp.Discrete(["softmax"]))
HP_OPTIMIZER = hp.HParam('optimizer', hp.Discrete(["rmsprop"]))
HP_EPOCHS = hp.HParam('epochs', hp.Discrete([1]))
HP_LAG = hp.HParam('lag', hp.Discrete([13]))

METRIC_ACCURACY = 'accuracy'


with tf.summary.create_file_writer('logs/hparam_tuning/' + dateAndTimeNow).as_default():
    hp.hparams_config(
        hparams=[HP_NUM_UNITS, HP_BATCH_SIZE, HP_ACT_FUNC,HP_EPOCHS,HP_LAG],
        metrics=[hp.Metric(METRIC_ACCURACY, display_name='Accuracy')],
    )


def train_test_model2(hparams):
    class_names = ["Non Aggressive Event", "Aggresive Right Turn", "Aggresive Left Turn", "Aggresive Acceleration",
                   "Aggresive Braking", "Aggresive Left Lane Change", "Aggresive Right Lane Change"]
    def log_confusion_matrix(epoch, logs):
        # Use the model to predict the values from the validation dataset.
        test_pred_raw = model.predict(X_test)
        test_pred = np.argmax(test_pred_raw, axis=1)
        y_test2 = np.argmax(y_test,axis=1)
        # Calculate the confusion matrix.
        cm = sklearn.metrics.confusion_matrix(y_test2, test_pred)
        # Log the confusion matrix as an image summary.
        figure = plot_confusion_matrix(cm, class_names=class_names)
        cm_image = plot_to_image(figure)

        # Log the confusion matrix as an image summary.
        with file_writer_cm.as_default():
            tf.summary.image("Confusion Matrix", cm_image, step=epoch)

    # Define the per-epoch callback.
    cm_callback = LambdaCallback(on_epoch_end=log_confusion_matrix)

    input = Input(shape=(lag, 3))
    input_branch_outputs = []
    hidden_branch_outputs = []
    for i in range(3):
        # Slicing the ith channel:
        #out = Lambda(lambda x: x[:, i, :])(input)

        # Setting up your per-channel layers (replace with actual sub-models):
        out = GRU(units=10, activation=hparams[HP_ACT_FUNC],return_sequences=True)(input)
        input_branch_outputs.append(out)

    # Concatenating together the per-channel results:
    inputLayerOutput = Concatenate()(input_branch_outputs)

    for i in range(10):
        # Slicing the ith channel:
        #out = Lambda(lambda x: x[:, i, :])(input)

        # Setting up your per-channel layers (replace with actual sub-models):
        out = GRU(units=7, activation=hparams[HP_ACT_FUNC])(inputLayerOutput)
        hidden_branch_outputs.append(out)

    # Concatenating together the per-channel results:
    hiddenLayerOutput = Concatenate()(hidden_branch_outputs)

    # Adding some further layers (replace or remove with your architecture):
    out = Dense(units=7, activation=hparams[HP_ACT_FUNC])(hiddenLayerOutput)

    # Building model:
    model = Model(inputs=input, outputs=out)
    model.compile(loss='mean_squared_error', optimizer=hparams[HP_OPTIMIZER], metrics=['acc'])
    plot_model(model, to_file='model_plot' + dateAndTimeNow + '.png', show_shapes=True, show_layer_names=True)

    model.fit(X_train, y_train, epochs=hparams[HP_EPOCHS], batch_size=hparams[HP_BATCH_SIZE], callbacks=[
        tf.keras.callbacks.TensorBoard(
            "logs/fit/" + dateAndTimeNow),cm_callback])  # Run with 1 epoch to speed things up for demo purposes
    y_pred = model.predict(X_test)

    y_test_argmax = np.argmax(y_test, axis=1)
    y_pred = np.round(y_pred)
    sums = np.sum(y_pred,axis=1)


    numberOfInsignificantEvents = 0

    for sum in sums:
        if sum == 0:
            numberOfInsignificantEvents =  numberOfInsignificantEvents+ 1

    y_pred_argmax = np.argmax(y_pred, axis=1)
    print(sklearn.metrics.multilabel_confusion_matrix(y_test_argmax, y_pred_argmax))

    _, accuracy = model.evaluate(X_test, y_test)

    complete_parameters = "Batch Size-" + str(hparams[HP_BATCH_SIZE]) + "LAG-" + str(hparams[HP_LAG]) + "Activation Function-" + str(hparams[HP_ACT_FUNC]) + "Optimizer-" + str(hparams[HP_OPTIMIZER])

    dir_name = "CSVFiles/RNNLinearAcc-" + complete_parameters + str(datetime.datetime.now().strftime("%m.%d.%Y, %H.%M.%S"))
    os.mkdir(dir_name)

    save_to_csv_file(dir_name,y_train,y_test,y_pred,y_test_argmax,y_pred_argmax)
    save_data(dir_name,X_train,X_test,y_train,y_test)

    convertModelToTFLITE(model, "ExactCopy5-Acc" + str(accuracy))

    print("Insignificant Events ==" , numberOfInsignificantEvents)
    return accuracy


def run(run_dir, hparams):
    with tf.summary.create_file_writer(run_dir).as_default():
        hp.hparams(hparams)  # record the values used in this trial
        accuracy = train_test_model2(hparams)
        tf.summary.scalar(METRIC_ACCURACY, accuracy, step=1)


session_num = 0

for num_units in HP_NUM_UNITS.domain.values:
    for batch_size in HP_BATCH_SIZE.domain.values:
        for act_func in HP_ACT_FUNC.domain.values:
            for optimizer in HP_OPTIMIZER.domain.values:
                for epochs in HP_EPOCHS.domain.values:
                    for lag in HP_LAG.domain.values:
                        hparams = {
                            HP_NUM_UNITS: num_units,
                            HP_BATCH_SIZE: batch_size,
                            HP_ACT_FUNC: act_func,
                            HP_OPTIMIZER: optimizer,
                            HP_EPOCHS: epochs,
                            HP_LAG: lag,
                        }

                        if path.exists("X"+str(lag-1) + "AccData") == True:
                            X_orig = pd.read_pickle("X"+str(lag-1) + "AccData")
                            y_orig = pd.read_pickle("y"+str(lag-1) + "AccData")
                        else:
                            # Obtaining the csv file having all outputs on 1 coloumn
                            X_orig,y_orig = returnXYData(lag-1, 5)

                            X_orig.to_pickle("X"+str(lag-1) + "AccData")
                            y_orig.to_pickle("y"+str(lag-1) + "AccData")

                        # X = X49LagAccData
                        # y= y49lagAccData

                        #X = pd.read_pickle("../InputPickleFiles/")
                        #y = pd.read_pickle("../InputPickleFiles/49LagAccOnly/y49lagAccData")

                        X_train, X_test, y_train, y_test = train_test_split(X_orig, y_orig, test_size=0.2)

                        X_train = X_train.to_numpy()
                        y_train = y_train.to_numpy()
                        X_test = X_test.to_numpy()
                        y_test = y_test.to_numpy()

                        # X_train = np.reshape(X_train, (X_train.shape[0], X_train.shape[1], 1))
                        # X_test = np.reshape(X_test, (X_test.shape[0], X_test.shape[1], 1))
                        X_train = convertToIndividualArray(X_train, lag)
                        X_test = convertToIndividualArray(X_test, lag)

                        run_name = "run-%d" % session_num
                        print('--- Starting trial: %s' % run_name)
                        print({h.name: hparams[h] for h in hparams})
                        run('logs/hparam_tuning/' + dateAndTimeNow + run_name, hparams)
                        session_num += 1
