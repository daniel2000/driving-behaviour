# https://www.datatechnotes.com/2018/12/rnn-example-with-keras-simplernn-in.html
import datetime
import io
import itertools

import sklearn
from keras.layers import GRU
from sklearn.model_selection import train_test_split
from tensorflow.python.keras.utils.vis_utils import plot_model
from tensorboard.plugins.hparams import api as hp
from keras.callbacks import LambdaCallback
from Helper.HelperClass import *
import tensorflow as tf

from Helper.FormatAndObtainData import *
from keras.layers import Dense, Concatenate, Input
from keras.models import Model

from Helper.ModelConverterToTFLITE import convertModelToTFLITE
import matplotlib.pyplot as plt
"""
Proposed RNN topology. 
The inputs take into account THREE VECTORS of 50 values each. 
There is ONE HIDDEN layer with RECURRENCE (GRU) (with 2, 5 and 10 neurons). 

The output is mapped as a multi-label problem; 
each output neuron is linked to an event type, as shown in Table I.
"""

def plot_to_image(figure):
  """Converts the matplotlib plot specified by 'figure' to a PNG image and
  returns it. The supplied figure is closed and inaccessible after this call."""
  # Save the plot to a PNG in memory.
  buf = io.BytesIO()
  plt.savefig(buf, format='png')
  # Closing the figure prevents it from being displayed directly inside
  # the notebook.
  plt.close(figure)
  buf.seek(0)
  # Convert PNG buffer to TF image
  image = tf.image.decode_png(buf.getvalue(), channels=4)
  # Add the batch dimension
  image = tf.expand_dims(image, 0)
  return image

def plot_confusion_matrix(cm, class_names):
    """
    Returns a matplotlib figure containing the plotted confusion matrix.

    Args:
    cm (array, shape = [n, n]): a confusion matrix of integer classes
    class_names (array, shape = [n]): String names of the integer classes
    """
    figure = plt.figure(figsize=(8, 8))
    plt.imshow(cm, interpolation='nearest', cmap=plt.cm.Blues)
    plt.title("Confusion matrix")
    plt.colorbar()
    tick_marks = np.arange(len(class_names))
    plt.xticks(tick_marks, class_names, rotation=45)
    plt.yticks(tick_marks, class_names)



    # Compute the labels from the normalized confusion matrix.
    labels = np.around(cm.astype('float') / cm.sum(axis=1)[:, np.newaxis], decimals=2)


    # Use white text if squares are dark; otherwise black.
    threshold = cm.max() / 2.
    for i, j in itertools.product(range(cm.shape[0]), range(cm.shape[1])):
        color = "white" if cm[i, j] > threshold else "black"
        temp = str(cm[i, j]) + "," + str(labels[i,j])
        plt.text(j, i, temp, horizontalalignment="center", color=color)

    plt.tight_layout()
    plt.ylabel('True label')
    plt.xlabel('Predicted label')
    return figure





# convert into dataset matrix
def convertToMatrix(data, step):
    X, Y = [], []
    for i in range(len(data) - step):
        d = i + step
        X.append(data[i:d, ])
        Y.append(data[d,])
    return np.array(X), np.array(Y)


def convertToIndividualArray(X_train):
    temp = np.empty((X_train.shape[0], numofLag, 3))
    for rowNo, row in enumerate(X_train):
        for colNo in range(0, len(row), 3):
            temp2 = int(colNo / 3)
            temp[rowNo, temp2, 0] = X_train[rowNo, colNo]
            temp[rowNo, temp2, 1] = X_train[rowNo, colNo + 1]
            temp[rowNo, temp2, 2] = X_train[rowNo, colNo + 2]

    return temp


N = 1000
Tp = 800
epochs = 350
noOfNeuronsInOutputLayer = 7
dateAndTimeNow = datetime.datetime.now().strftime("%Y%m%d-%H%M%S")

# Obtaining the csv file having all outputs on 1 coloumn
X49LagLinearAcc10HzData,y49LagLinearAcc10HzData = returnXYData(4, 5)

X49LagLinearAcc10HzData.to_pickle("X4LagLinearAcc10HzData")
y49LagLinearAcc10HzData.to_pickle("y4LagLinearAcc10HzData")

numofLag = 4+1
#X = pd.read_pickle("../InputPickleFiles/49LagLinearAccDataWithMyData/X49LinearAccDataWithMyData")
#y = pd.read_pickle("../InputPickleFiles/49LagLinearAccDataWithMyData/y49LinearAccDataWithMyData")

X_train, X_test, y_train, y_test = train_test_split(X49LagLinearAcc10HzData, y49LagLinearAcc10HzData, test_size=0.2)

X_train = X_train.to_numpy()
y_train = y_train.to_numpy()
X_test = X_test.to_numpy()
y_test = y_test.to_numpy()

#X_train = np.reshape(X_train, (X_train.shape[0], X_train.shape[1], 1))
#X_test = np.reshape(X_test, (X_test.shape[0], X_test.shape[1], 1))
X_train = convertToIndividualArray(X_train)
X_test = convertToIndividualArray(X_test)


logdir = "logs/image/" + dateAndTimeNow
# Define the basic TensorBoard callback.
tensorboard_callback = tf.keras.callbacks.TensorBoard(log_dir=logdir)
file_writer_cm = tf.summary.create_file_writer(logdir + '/cm')



step = X_train.shape[1]

HP_NUM_UNITS = hp.HParam('num_units', hp.Discrete([7]))
HP_BATCH_SIZE = hp.HParam('batch_size', hp.Discrete([32]))
HP_ACT_FUNC = hp.HParam('activation_function', hp.Discrete(["softmax"]))
HP_OPTIMIZER = hp.HParam('optimizer', hp.Discrete(["rmsprop"]))

METRIC_ACCURACY = 'accuracy'

with tf.summary.create_file_writer('logs/hparam_tuning/' + dateAndTimeNow).as_default():
    hp.hparams_config(
        hparams=[HP_NUM_UNITS, HP_BATCH_SIZE, HP_ACT_FUNC],
        metrics=[hp.Metric(METRIC_ACCURACY, display_name='Accuracy')],
    )


def train_test_model2(hparams):
    class_names = ["Non Aggressive Event", "Aggresive Right Turn", "Aggresive Left Turn", "Aggresive Acceleration",
                   "Aggresive Braking", "Aggresive Left Lane Change", "Aggresive Right Lane Change"]

    def log_confusion_matrix(epoch, logs):
        # Use the model to predict the values from the validation dataset.
        test_pred_raw = model.predict(X_test)
        test_pred = np.argmax(test_pred_raw, axis=1)
        y_test2 = np.argmax(y_test, axis=1)
        # Calculate the confusion matrix.
        cm = sklearn.metrics.confusion_matrix(y_test2, test_pred)
        # Log the confusion matrix as an image summary.
        figure = plot_confusion_matrix(cm, class_names=class_names)
        cm_image = plot_to_image(figure)

        # Log the confusion matrix as an image summary.
        with file_writer_cm.as_default():
            tf.summary.image("Confusion Matrix", cm_image, step=epoch)

    # Define the per-epoch callback.
    cm_callback = LambdaCallback(on_epoch_end=log_confusion_matrix)

    input = Input(shape=(numofLag, 3))
    branch_outputs = []
    for i in range(10):
        # Slicing the ith channel:
        #out = Lambda(lambda x: x[:, i, :])(input)

        # Setting up your per-channel layers (replace with actual sub-models):
        out = GRU(units=7, activation="softmax")(input)
        branch_outputs.append(out)

    # Concatenating together the per-channel results:
    out = Concatenate()(branch_outputs)

    # Adding some further layers (replace or remove with your architecture):
    out = Dense(units=7, activation="softmax")(out)

    # Building model:
    model = Model(inputs=input, outputs=out)
    model.compile(loss='mean_squared_error', optimizer="rmsprop", metrics=['acc'])
    plot_model(model, to_file='model_plot' + dateAndTimeNow + '.png', show_shapes=True, show_layer_names=True)

    model.fit(X_train, y_train, epochs=epochs, batch_size=hparams[HP_BATCH_SIZE], callbacks=[
        tf.keras.callbacks.TensorBoard(
            "logs/fit/" + dateAndTimeNow), cm_callback])  # Run with 1 epoch to speed things up for demo purposes
    test_predictions = model.predict(X_test)

    y_test2 = np.argmax(y_test, axis=1)
    test_predictions2 = np.argmax(test_predictions, axis=1)
    print(sklearn.metrics.multilabel_confusion_matrix(y_test2, test_predictions2))

    _, accuracy = model.evaluate(X_test, y_test)

    convertModelToTFLITE(model, "ExactCopy4-Acc" + str(accuracy))
    return accuracy


"""
def train_test_model(hparams):
    model = Sequential()
    # model.add(Dense(input_shape=(step, 1), units=hparams[HP_NUM_UNITS], activation=hparams[HP_ACT_FUNC]))
    model.add(GRU(input_shape=(step, 1), return_sequences=True, units=3, activation="softmax"))
    model.add(GRU(10, activation="softmax"))
    model.add(Dense(hparams[HP_NUM_UNITS], activation="sigmoid"))
    model.compile(loss='mean_squared_error', optimizer=hparams[HP_OPTIMIZER], metrics=['acc'])
    plot_model(model, to_file='model_plot' + dateAndTimeNow + '.png', show_shapes=True, show_layer_names=True)

    model.fit(X_train, y_train, epochs=epochs, batch_size=hparams[HP_BATCH_SIZE], callbacks=[
        tf.keras.callbacks.TensorBoard(
            "logs/fit/" + dateAndTimeNow)])  # Run with 1 epoch to speed things up for demo purposes
    _, accuracy = model.evaluate(X_test, y_test)

    return accuracy
"""

def run(run_dir, hparams):
    with tf.summary.create_file_writer(run_dir).as_default():
        hp.hparams(hparams)  # record the values used in this trial
        accuracy = train_test_model2(hparams)
        tf.summary.scalar(METRIC_ACCURACY, accuracy, step=1)


session_num = 0

for num_units in HP_NUM_UNITS.domain.values:
    for batch_size in HP_BATCH_SIZE.domain.values:
        for act_func in HP_ACT_FUNC.domain.values:
            for optimizer in HP_OPTIMIZER.domain.values:
                hparams = {
                    HP_NUM_UNITS: num_units,
                    HP_BATCH_SIZE: batch_size,
                    HP_ACT_FUNC: act_func,
                    HP_OPTIMIZER: optimizer,
                }
                run_name = "run-%d" % session_num
                print('--- Starting trial: %s' % run_name)
                print({h.name: hparams[h] for h in hparams})
                run('logs/hparam_tuning/' + dateAndTimeNow + run_name, hparams)
                session_num += 1
