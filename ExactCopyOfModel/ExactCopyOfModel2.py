# https://www.datatechnotes.com/2018/12/rnn-example-with-keras-simplernn-in.html
import datetime

from keras.layers import GRU
from sklearn.model_selection import train_test_split
from tensorflow.python.keras.utils.vis_utils import plot_model
from tensorboard.plugins.hparams import api as hp

from Helper.HelperClass import *
import tensorflow as tf

from Helper.FormatAndObtainData import *
from keras.layers import Dense, Concatenate, Input
from keras.models import Model

from Helper.ModelConverterToTFLITE import convertModelToTFLITE

"""
Proposed RNN topology. 
The inputs take into account THREE VECTORS of 50 values each. 
There is ONE HIDDEN layer with RECURRENCE (GRU) (with 2, 5 and 10 neurons). 

The output is mapped as a multi-label problem; 
each output neuron is linked to an event type, as shown in Table I.
"""


# convert into dataset matrix
def convertToMatrix(data, step):
    X, Y = [], []
    for i in range(len(data) - step):
        d = i + step
        X.append(data[i:d, ])
        Y.append(data[d,])
    return np.array(X), np.array(Y)


def convertToIndividualArray(X_train):
    temp = np.empty((X_train.shape[0], 50, 3))
    for rowNo, row in enumerate(X_train):
        for colNo in range(0, len(row), 3):
            temp2 = int(colNo / 3)
            temp[rowNo, temp2, 0] = X_train[rowNo, colNo]
            temp[rowNo, temp2, 1] = X_train[rowNo, colNo + 1]
            temp[rowNo, temp2, 2] = X_train[rowNo, colNo + 2]

    return temp


N = 1000
Tp = 800
epochs = 1000
noOfNeuronsInOutputLayer = 7
dateAndTimeNow = datetime.datetime.now().strftime("%Y%m%d-%H%M%S")

# Obtaining the csv file having all outputs on 1 coloumn
# X49LagAccData,y49lagAccData = returnXYData(49, 5)

# X49LagAccData.to_pickle("X49LagAccData")
# y49lagAccData.to_pickle("y49lagAccData")

X = pd.read_pickle("../InputPickleFiles/49LagAccOnly/X49LagAccData")
y = pd.read_pickle("../InputPickleFiles/49LagAccOnly/y49lagAccData")

X_train, X_test, y_train, y_test = train_test_split(X, y, test_size=0.3)

X_train = X_train.to_numpy()
y_train = y_train.to_numpy()
X_test = X_test.to_numpy()
y_test = y_test.to_numpy()

#X_train = np.reshape(X_train, (X_train.shape[0], X_train.shape[1], 1))
#X_test = np.reshape(X_test, (X_test.shape[0], X_test.shape[1], 1))
X_train = convertToIndividualArray(X_train)
X_test = convertToIndividualArray(X_test)


step = X_train.shape[1]

HP_NUM_UNITS = hp.HParam('num_units', hp.Discrete([7]))
HP_BATCH_SIZE = hp.HParam('batch_size', hp.Discrete([10]))
HP_ACT_FUNC = hp.HParam('activation_function', hp.Discrete(["softmax"]))
HP_OPTIMIZER = hp.HParam('optimizer', hp.Discrete(["rmsprop"]))

METRIC_ACCURACY = 'accuracy'

with tf.summary.create_file_writer('logs/hparam_tuning/' + dateAndTimeNow).as_default():
    hp.hparams_config(
        hparams=[HP_NUM_UNITS, HP_BATCH_SIZE, HP_ACT_FUNC],
        metrics=[hp.Metric(METRIC_ACCURACY, display_name='Accuracy')],
    )


def train_test_model2(hparams):
    input = Input(shape=(50, 3))
    branch_outputs = []
    for i in range(10):
        # Slicing the ith channel:
        #out = Lambda(lambda x: x[:, i, :])(input)

        # Setting up your per-channel layers (replace with actual sub-models):
        out = GRU(units=7, activation="sigmoid")(input)
        branch_outputs.append(out)

    # Concatenating together the per-channel results:
    out = Concatenate()(branch_outputs)

    # Adding some further layers (replace or remove with your architecture):
    out = Dense(units=7, activation="sigmoid")(out)

    # Building model:
    model = Model(inputs=input, outputs=out)
    model.compile(loss='mean_squared_error', optimizer="rmsprop", metrics=['acc'])
    plot_model(model, to_file='model_plot' + dateAndTimeNow + '.png', show_shapes=True, show_layer_names=True)

    model.fit(X_train, y_train, epochs=epochs, batch_size=hparams[HP_BATCH_SIZE], callbacks=[
        tf.keras.callbacks.TensorBoard(
            "logs/fit/" + dateAndTimeNow)])  # Run with 1 epoch to speed things up for demo purposes
    test_predictions = model.predict(X_test)
    _, accuracy = model.evaluate(X_test, y_test)

    convertModelToTFLITE(model, "Copy1-Acc" + str(accuracy))
    return accuracy


"""
def train_test_model(hparams):
    model = Sequential()
    # model.add(Dense(input_shape=(step, 1), units=hparams[HP_NUM_UNITS], activation=hparams[HP_ACT_FUNC]))
    model.add(GRU(input_shape=(step, 1), return_sequences=True, units=3, activation="softmax"))
    model.add(GRU(10, activation="softmax"))
    model.add(Dense(hparams[HP_NUM_UNITS], activation="sigmoid"))
    model.compile(loss='mean_squared_error', optimizer=hparams[HP_OPTIMIZER], metrics=['acc'])
    plot_model(model, to_file='model_plot' + dateAndTimeNow + '.png', show_shapes=True, show_layer_names=True)

    model.fit(X_train, y_train, epochs=epochs, batch_size=hparams[HP_BATCH_SIZE], callbacks=[
        tf.keras.callbacks.TensorBoard(
            "logs/fit/" + dateAndTimeNow)])  # Run with 1 epoch to speed things up for demo purposes
    _, accuracy = model.evaluate(X_test, y_test)

    return accuracy
"""

def run(run_dir, hparams):
    with tf.summary.create_file_writer(run_dir).as_default():
        hp.hparams(hparams)  # record the values used in this trial
        accuracy = train_test_model2(hparams)
        tf.summary.scalar(METRIC_ACCURACY, accuracy, step=1)


session_num = 0

for num_units in HP_NUM_UNITS.domain.values:
    for batch_size in HP_BATCH_SIZE.domain.values:
        for act_func in HP_ACT_FUNC.domain.values:
            for optimizer in HP_OPTIMIZER.domain.values:
                hparams = {
                    HP_NUM_UNITS: num_units,
                    HP_BATCH_SIZE: batch_size,
                    HP_ACT_FUNC: act_func,
                    HP_OPTIMIZER: optimizer,
                }
                run_name = "run-%d" % session_num
                print('--- Starting trial: %s' % run_name)
                print({h.name: hparams[h] for h in hparams})
                run('logs/hparam_tuning/' + dateAndTimeNow + run_name, hparams)
                session_num += 1
