# https://www.datatechnotes.com/2018/12/rnn-example-with-keras-simplernn-in.html
import numpy as np
import matplotlib.pyplot as plt
from keras.models import Sequential
from keras.layers import Dense, LSTM
from sklearn.model_selection import train_test_split
from sklearn.metrics import confusion_matrix, precision_score
from keras.callbacks import History

from Helper.FormatAndObtainData import *


# convert into dataset matrix
def convertToMatrix(data, step):
    X, Y = [], []
    for i in range(len(data) - step):
        d = i + step
        X.append(data[i:d, ])
        Y.append(data[d,])
    return np.array(X), np.array(Y)


step = 108
N = 1000
Tp = 800

#t = np.arange(0, N)
#x = np.sin(0.02 * t) + 2 * np.random.rand(N)
#df = pd.DataFrame(x)
#df.head()

#plt.plot(df)
#plt.show()

#values = df.values
#train, test = values[0:Tp, :], values[Tp:N, :]

# add step elements into train and test
#test = np.append(test, np.repeat(test[-1,], step))
#train = np.append(train, np.repeat(train[-1,], step))

#trainX, trainY = convertToMatrix(train, step)
#testX, testY = convertToMatrix(test, step)
#trainX = np.reshape(trainX, (trainX.shape[0], 1, trainX.shape[1]))
#testX = np.reshape(testX, (testX.shape[0], 1, testX.shape[1]))
step = 108
X,y, le, noOfClasses = returnXYData()
epochs = 1000
noOfNeurons = 10

X_train, X_test, y_train, y_test = train_test_split(X, y, test_size=0.3)

model = Sequential()
model.add(LSTM(units=32, input_shape=(1, step), activation="softmax"))
model.add(Dense(noOfNeurons, activation="softmax"))
model.add(Dense(1))
model.compile(loss='mean_squared_error', optimizer='rmsprop',metrics=['acc'])
model.summary()


X_train = X_train.to_numpy()
y_train = y_train.to_numpy()
X_test = X_test.to_numpy()
y_test = y_test.to_numpy()

X_train = np.reshape(X_train, (X_train.shape[0], 1, X_train.shape[1]))
X_test = np.reshape(X_test, (X_test.shape[0], 1, X_test.shape[1]))

history = History()


modelOutput = model.fit(X_train, y_train, epochs=epochs, batch_size=16, verbose=2)
trainPredict = model.predict(X_train)
testPredict = model.predict(X_test)
predicted = np.concatenate((trainPredict, testPredict), axis=0)

trainScore = model.evaluate(X_train, y_train, verbose=0)
print(trainScore)

testPredict = np.rint(testPredict)

uniqueValues = np.unique(testPredict)

print("Predicted Values = " , len(uniqueValues))
for i in uniqueValues:
    print(i)


print("Confusion Matrix: x-axis = Predicated Label, y-axis = True Label")
print(confusion_matrix(y_test, testPredict))
print("Precision Score = " , precision_score(y_test, testPredict,average="weighted"))
print("The precision is intuitively the ability of the classifier not to label as positive a sample that is negative.")

#Loss is calculated base on the train set, val_loss is calculated base on the validation set.
plt.plot(range(epochs), modelOutput.history['loss'])
plt.plot(range(epochs), modelOutput.history['acc'])
plt.title(label="Epochs =" + str(epochs) + ", No of neurons = " + str(noOfNeurons))
plt.show()
