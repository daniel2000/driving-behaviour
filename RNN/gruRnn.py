# https://www.datatechnotes.com/2018/12/rnn-example-with-keras-simplernn-in.html
import datetime

import matplotlib.pyplot as plt
from keras.models import Sequential
from keras.layers import Dense, GRU
from keras.callbacks import History
from tensorflow.python.keras.utils.vis_utils import plot_model

from Helper.HelperClass import  *
import tensorflow as tf


# convert into dataset matrix
def convertToMatrix(data, step):
    X, Y = [], []
    for i in range(len(data) - step):
        d = i + step
        X.append(data[i:d, ])
        Y.append(data[d,])
    return np.array(X), np.array(Y)



N = 1000
Tp = 800

#t = np.arange(0, N)
#x = np.sin(0.02 * t) + 2 * np.random.rand(N)
#df = pd.DataFrame(x)
#df.head()

#plt.plot(df)
#plt.show()

#values = df.values
#train, test = values[0:Tp, :], values[Tp:N, :]

# add step elements into train and test
#test = np.append(test, np.repeat(test[-1,], step))
#train = np.append(train, np.repeat(train[-1,], step))

#trainX, trainY = convertToMatrix(train, step)
#testX, testY = convertToMatrix(test, step)
#trainX = np.reshape(trainX, (trainX.shape[0], 1, trainX.shape[1]))
#testX = np.reshape(testX, (testX.shape[0], 1, testX.shape[1]))


def getGRURNNModel(X_train, X_test, y_train, y_test, epochs,noOfNeuronsInMiddleLayer,noOfNeuronsInOutputLayer):
    X_train = X_train.to_numpy()
    y_train = y_train.to_numpy()
    X_test = X_test.to_numpy()
    y_test = y_test.to_numpy()

    X_train = np.reshape(X_train, (X_train.shape[0],X_train.shape[1],1 ))
    X_test = np.reshape(X_test, (X_test.shape[0],  X_test.shape[1], 1))


    step = X_train.shape[1]

    #epochs = 1000
    #noOfNeurons = 10
    """
    model = Sequential()
    model.add(Dense(input_shape=(step,1), units=10,activation="softmax"))
    model.add(GRU(units=20, activation="softmax"))
    model.add(Dense(noOfNeuronsInOutputLayer,activation="softmax"))
    model.compile(loss='mean_squared_error', optimizer='rmsprop', metrics=['acc'])
    model.summary()
    
    """

    """
    # 0.93 Batch size of 1 
    model = Sequential()
    model.add(Dense(input_shape=(step,1), units=10,activation="softmax"))
    model.add(GRU(units=20, activation="softmax"))
    model.add(Dense(noOfNeuronsInOutputLayer,activation="softmax"))
    model.compile(loss='mean_squared_error', optimizer='rmsprop', metrics=['acc'])
    model.summary()
    """


    #Best 0.96 
    
    model = Sequential()
    model.add(Dense(input_shape=(step,1), units=10,activation="softmax"))
    model.add(GRU(units=20, activation="softmax"))
    model.add(Dense(noOfNeuronsInOutputLayer,activation="softmax"))
    model.compile(loss='mean_squared_error', optimizer='rmsprop', metrics=['acc'])
    model.summary()


    """
    # 0.94
    model = Sequential()
    model.add(Dense(input_shape=(step, 1), units=20, activation="softmax"))
    model.add(GRU(units=10, activation="softmax"))
    model.add(Dense(noOfNeuronsInOutputLayer, activation="softmax"))
    model.compile(loss='mean_squared_error', optimizer='rmsprop', metrics=['acc'])
    model.summary()
    """

    """
    #0.91
    model = Sequential()
    model.add(Dense(input_shape=(step, 1), units=50, activation="softmax"))
    model.add(GRU(units=10, activation="softmax"))
    model.add(Dense(noOfNeuronsInOutputLayer, activation="softmax"))
    model.compile(loss='mean_squared_error', optimizer='rmsprop', metrics=['acc'])
    model.summary()
    """

    """
    #0.9589 = 0.96z
    model = Sequential()
    model.add(Dense(input_shape=(step, 1), units=50, activation="softmax"))
    model.add(GRU(units=50, activation="softmax"))
    model.add(Dense(noOfNeuronsInOutputLayer, activation="softmax"))
    model.compile(loss='mean_squared_error', optimizer='rmsprop', metrics=['acc'])
    model.summary()
    """

    """
    #0.74
    model = Sequential()
    model.add(Dense(input_shape=(step, 1), units=50,  activation="softmax"))
    model.add(GRU(units=50, activation="softmax",recurrent_activation="softmax",recurrent_dropout = 0,unroll=False,use_bias=True,reset_after=True))
    model.add(Dense(noOfNeuronsInOutputLayer, activation="softmax"))
    model.compile(loss='mean_squared_error', optimizer='rmsprop', metrics=['acc'])
    model.summary()
    """



    """
    #0.85
    model = Sequential()
    model.add(GRU(units=20,input_shape=(step,1), activation="softmax"))
    model.add(Dense(noOfNeuronsInMiddleLayer, activation="softmax"))
    model.add(Dense(noOfNeuronsInOutputLayer, activation="softmax"))
    model.compile(loss='mean_squared_error', optimizer='rmsprop', metrics=['acc'])
    model.summary()
    """



    history = History()

    modelOutput = model.fit(X_train, y_train, epochs=epochs, verbose=2,batch_size=1)

    trainPredict = model.predict(X_train)
    testPredict = model.predict(X_test)
    predicted = np.concatenate((trainPredict, testPredict), axis=0)

    trainScore = model.evaluate(X_train, y_train, verbose=0)
    print(trainScore)

    testPredictInt = np.rint(testPredict)

    uniqueValues = np.unique(testPredictInt)

    print("Predicted Values = ", len(uniqueValues))
    for i in uniqueValues:
        print(i)

    print("Confusion Matrix: x-axis = Predicated Label, y-axis = True Label")
    #   confMatrix = confusion_matrix(y_test, testPredictInt)
    #print(confMatrix)
    #print("Precision Score = ", precision_score(y_test, testPredictInt, average="weighted"))
    #print("Custom Micro Precision", calculateMicroPrecision(confMatrix))

    # Loss is calculated base on the train set, val_loss is calculated base on the validation set.
    plt.plot(range(epochs), modelOutput.history['loss'],label="loss")
    plt.plot(range(epochs), modelOutput.history['acc'],label="acc")
    plt.legend(loc="upper left")
    plt.title(label="Epochs =" + str(epochs) + "No of neurons = " + str(noOfNeuronsInMiddleLayer))
    plt.show()

    return testPredict,model

def getGRURNNModelNEW(X_train, X_test, y_train, y_test, epochs,noOfNeuronsInMiddleLayer,noOfNeuronsInOutputLayer):
    step = X_train[0].shape[1]
    #epochs = 1000
    #noOfNeurons = 10



    model = Sequential()
    model.add(GRU(units=7,input_shape=(1,step),recurrent_activation="softmax", activation="softmax"))
    model.add(Dense(noOfNeuronsInMiddleLayer, activation="softmax"))
    model.add(Dense(noOfNeuronsInOutputLayer, activation="softmax"))
    model.compile(loss='mean_squared_error', optimizer='rmsprop', metrics=['acc'])

    model.summary()




    history = History()
    for x,y in zip(X_train,y_train):
        X_train = x.to_numpy()
        y_train = y.to_numpy()
        X_test = X_test.to_numpy()
        y_test = y_test.to_numpy()

        X_train = np.reshape(X_train, (X_train.shape[0], 1, X_train.shape[1]))
        X_test = np.reshape(X_test, (X_test.shape[0], 1, X_test.shape[1]))

        modelOutput = model.fit(x, y, epochs=epochs, batch_size=16, verbose=2)

    trainPredict = model.predict(X_train)
    testPredict = model.predict(X_test)
    predicted = np.concatenate((trainPredict, testPredict), axis=0)

    trainScore = model.evaluate(X_train, y_train, verbose=0)
    print(trainScore)

    testPredictInt = np.rint(testPredict)

    uniqueValues = np.unique(testPredictInt)

    print("Predicted Values = ", len(uniqueValues))
    for i in uniqueValues:
        print(i)

    print("Confusion Matrix: x-axis = Predicated Label, y-axis = True Label")
    #   confMatrix = confusion_matrix(y_test, testPredictInt)
    #print(confMatrix)
    #print("Precision Score = ", precision_score(y_test, testPredictInt, average="weighted"))
    #print("Custom Micro Precision", calculateMicroPrecision(confMatrix))

    # Loss is calculated base on the train set, val_loss is calculated base on the validation set.
    plt.plot(range(epochs), modelOutput.history['loss'],label="loss")
    plt.plot(range(epochs), modelOutput.history['acc'],label="acc")
    plt.legend(loc="upper left")
    plt.title(label="Epochs =" + str(epochs) + "No of neurons = " + str(noOfNeuronsInMiddleLayer))
    plt.show()

    return testPredict


def getGRURNN2(X_train, X_test, y_train, y_test, epochs,noOfNeuronsInMiddleLayer,noOfNeuronsInOutputLayer):
    X_train = X_train.to_numpy()
    y_train = y_train.to_numpy()
    X_test = X_test.to_numpy()
    y_test = y_test.to_numpy()

    X_train = np.reshape(X_train, (X_train.shape[0], X_train.shape[1], 1))
    X_test = np.reshape(X_test, (X_test.shape[0], X_test.shape[1], 1))

    step = X_train.shape[1]

    # epochs = 1000
    # noOfNeurons = 10

    """
    #0.69 with batchsize 64
    model = Sequential()
    model.add(Dense(input_shape=(step, 1), units=3,activation="softmax"))
    model.add(GRU(units=7, activation="softmax"))
    #model.add(Dense(noOfNeuronsInOutputLayer,activation="softmax"))
    model.compile(loss='mean_squared_error', optimizer='rmsprop', metrics=['acc'])
    model.summary()
    """



    """
    # Best 0.96
    model = Sequential()
    model.add(Dense(input_shape=(step, 1), units=10, activation="softmax"))
    model.add(GRU(units=20, activation="softmax"))
    model.add(Dense(noOfNeuronsInOutputLayer, activation="softmax"))
    model.compile(loss='mean_squared_error', optimizer='rmsprop', metrics=['acc'])
    model.summary()
    #plot_model(model, to_file='model_plot.png', show_shapes=True, show_layer_names=True)
    """

    """
    # Best 0.96
    model = Sequential()
    model.add(Dense(input_shape=(step, 1), units=10, activation="softmax"))
    model.add(GRU(units=20, activation="softmax"))
    model.add(Dense(noOfNeuronsInOutputLayer, activation="softmax"))
    model.compile(loss='mean_squared_error', optimizer='rmsprop', metrics=['acc'])
    model.summary()
    plot_model(model, to_file='model_plot.png', show_shapes=True, show_layer_names=True)
    """

    # Best 0.96
    model = Sequential()
    model.add(Dense(input_shape=(step, 1), units=10, activation="softmax"))
    model.add(GRU(units=20, activation="softmax"))
    model.add(Dense(noOfNeuronsInOutputLayer, activation="softmax"))
    model.compile(loss='mean_squared_error', optimizer='rmsprop', metrics=['acc'])
    model.summary()
    plot_model(model, to_file='model_plot.png', show_shapes=True, show_layer_names=True)


    """
    #0.94 with batchsize 64
    model = Sequential()
    model.add(Dense(input_shape=(step, 1), units=10, activation="softmax"))
    model.add(GRU(units=20, activation="softmax"))
    model.add(Dense(noOfNeuronsInOutputLayer, activation="softmax"))
    model.compile(loss='mean_squared_error', optimizer='rmsprop', metrics=['acc'])
    model.summary()
    """

    """
    # 0.94
    model = Sequential()
    model.add(Dense(input_shape=(step, 1), units=20, activation="softmax"))
    model.add(GRU(units=10, activation="softmax"))
    model.add(Dense(noOfNeuronsInOutputLayer, activation="softmax"))
    model.compile(loss='mean_squared_error', optimizer='rmsprop', metrics=['acc'])
    model.summary()
    """

    """
    #0.91
    model = Sequential()
    model.add(Dense(input_shape=(step, 1), units=50, activation="softmax"))
    model.add(GRU(units=10, activation="softmax"))
    model.add(Dense(noOfNeuronsInOutputLayer, activation="softmax"))
    model.compile(loss='mean_squared_error', optimizer='rmsprop', metrics=['acc'])
    model.summary()
    """

    """
    #0.9589 = 0.96z
    model = Sequential()
    model.add(Dense(input_shape=(step, 1), units=50, activation="softmax"))
    model.add(GRU(units=50, activation="softmax"))
    model.add(Dense(noOfNeuronsInOutputLayer, activation="softmax"))
    model.compile(loss='mean_squared_error', optimizer='rmsprop', metrics=['acc'])
    model.summary()
    """

    """
    #0.74
    model = Sequential()
    model.add(Dense(input_shape=(step, 1), units=50,  activation="softmax"))
    model.add(GRU(units=50, activation="softmax",recurrent_activation="softmax",recurrent_dropout = 0,unroll=False,use_bias=True,reset_after=True))
    model.add(Dense(noOfNeuronsInOutputLayer, activation="softmax"))
    model.compile(loss='mean_squared_error', optimizer='rmsprop', metrics=['acc'])
    model.summary()
    """

    """
    #0.85
    model = Sequential()
    model.add(GRU(units=20,input_shape=(step,1), activation="softmax"))
    model.add(Dense(noOfNeuronsInMiddleLayer, activation="softmax"))
    model.add(Dense(noOfNeuronsInOutputLayer, activation="softmax"))
    model.compile(loss='mean_squared_error', optimizer='rmsprop', metrics=['acc'])
    model.summary()
    """

    log_dir = "logs/fit/" + datetime.datetime.now().strftime("%Y%m%d-%H%M%S")
    history = History()

    modelOutput = model.fit(X_train, y_train, epochs=epochs, verbose=2,callbacks=[tf.keras.callbacks.TensorBoard(log_dir)])

    trainPredict = model.predict(X_train)
    testPredict = model.predict(X_test)
    predicted = np.concatenate((trainPredict, testPredict), axis=0)

    trainScore = model.evaluate(X_train, y_train, verbose=0)
    print(trainScore)

    testPredictInt = np.rint(testPredict)

    uniqueValues = np.unique(testPredictInt)

    print("Predicted Values = ", len(uniqueValues))
    for i in uniqueValues:
        print(i)

    print("Confusion Matrix: x-axis = Predicated Label, y-axis = True Label")
    #   confMatrix = confusion_matrix(y_test, testPredictInt)
    # print(confMatrix)
    # print("Precision Score = ", precision_score(y_test, testPredictInt, average="weighted"))
    # print("Custom Micro Precision", calculateMicroPrecision(confMatrix))

    # Loss is calculated base on the train set, val_loss is calculated base on the validation set.
    plt.plot(range(epochs), modelOutput.history['loss'], label="loss")
    plt.plot(range(epochs), modelOutput.history['acc'], label="acc")
    plt.legend(loc="upper left")
    plt.title(label="Epochs =" + str(epochs) + "No of neurons = " + str(noOfNeuronsInMiddleLayer))
    plt.show()

    return testPredict, model
