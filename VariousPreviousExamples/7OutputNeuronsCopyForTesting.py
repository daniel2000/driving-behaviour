import tensorflow as tf

from RNN.gruRnn import *


epochs = 1000
test_size = 0.3
noOfNeuronsInMiddleLayer = 10
noOfNeuronsInOutputLayer = 7

"""
# Obtaining the csv file having all outputs on 1 coloumn
X50LagAccOnly,y50lagAccOnly = returnXYData(8, 5)

X50LagAccOnly.to_pickle("X8lagAccOnly")
y50lagAccOnly.to_pickle("y8lagAccOnly")
"""

X= pd.read_pickle("InputPickleFiles/50LagAccOnly/X50LagAccOnly")
y= pd.read_pickle("InputPickleFiles/50LagAccOnly/y50lagAccOnly")

X_train, X_test, y_train, y_test = train_test_split(X, y, test_size=test_size)

# Generating the model
predictions, model = getGRURNN2(X_train,X_test,y_train,y_test,epochs,noOfNeuronsInMiddleLayer,noOfNeuronsInOutputLayer)

predictions = binarizePredicitions(predictions)

np.set_printoptions(suppress=True)
print(multilabel_confusion_matrix(y_test,predictions))
print(precision_score(y_test,predictions,average="micro"))

# Print the precision and recall, among other metrics
print(metrics.classification_report(y_test, predictions, digits=4))

# Coverting the generated model to TFLite for Andriod Use
# Convert the model.
converter = tf.lite.TFLiteConverter.from_keras_model(model)
tflite_model = converter.convert()

# Save the model.
with open('../gruModel55.tflite', 'wb') as f:
  f.write(tflite_model)