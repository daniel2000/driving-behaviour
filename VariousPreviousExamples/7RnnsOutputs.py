import tempfile

from RNN.gruRnn import *



epochs = 1000 
test_size = 0.3
noOfNeuronsInMiddleLayer = 10
noOfNeuronsInOutputLayer = 1
predictionList = []

confusionMatrix = np.empty([7, 7])


np.set_printoptions(suppress=True)

#X50LagAccOnly,y50lagAccOnly = returnXYData(50,3,7)
X= pd.read_pickle("InputPickleFiles/50LagAccOnly/X50LagAccOnly")
y= pd.read_pickle("InputPickleFiles/50LagAccOnly/y50lagAccOnly")
X_train, X_test, y_train, y_test = train_test_split(X, y, test_size=test_size)

s = y_train.take([1], axis=1)
for i in range(7):
    predicitions, model = getGRURNNModel(X_train, X_test, y_train.take([i], axis=1), y_test.take([i], axis=1), epochs,
                   noOfNeuronsInMiddleLayer, noOfNeuronsInOutputLayer)
    predictionList.append(predicitions)




for i in range(len(predictionList[0])):
    max = -1
    maxColNo = -1
    for j in range(len(predictionList)):
        if predictionList[j][i][0] == max:
            print("EQUAL PREDICTIONS FOUND")
        if predictionList[j][i][0] > max:
            max = predictionList[j][i][0]
            maxColNo = j

    if y_test.iat[i, maxColNo] == 1:
        # Good Predication
        confusionMatrix[maxColNo,maxColNo] += 1
    else:
        for z in range(7):
            if y_test.iat[i, z] == 1:
                confusionMatrix[z,maxColNo] += 1
                break

            if confusionMatrix[z,maxColNo] > 1000:
                print("GEre")



print(confusionMatrix)
print("Final Mico Precision: ",calculateMicroPrecision(confusionMatrix))
#for i in range(7):
    #print("Precision Score = ", precision_score(y_test, testPredictInt, average="weighted"))







