import os
import tempfile
import tensorflow as tf


from sklearn.metrics import multilabel_confusion_matrix

from RNN.gruRnn import *
import statistics

epochs = 5
test_size = 0.3
noOfNeuronsInMiddleLayer = 10
noOfNeuronsInOutputLayer = 7


np.set_printoptions(suppress=True)

"""
# Obtaining the csv file having all outputs on 1 coloumn
X50LagAccOnly,y50lagAccOnly = returnXYData(8, 5)

X50LagAccOnly.to_pickle("X8lagAccOnly")
y50lagAccOnly.to_pickle("y8lagAccOnly")
"""

X= pd.read_pickle("InputPickleFiles/50LagAllData/X50LagAllData")
y= pd.read_pickle("InputPickleFiles/50LagAllData/y50lagAllData")

X_train, X_test, y_train, y_test = train_test_split(X, y, test_size=test_size)
NUMERIC_COLUMNS = list(X.columns)
test = X_train.iloc[0]
print(test)

file = open("test.txt","w")
for i in test:
    file.write(str(i))
    file.write("f,")

file.close()
#predictions, model = getGRURNNModelNEW(X_train,X_test,y_train,y_test,epochs,noOfNeuronsInMiddleLayer,noOfNeuronsInOutputLayer)
predictions,model = getGRURNN2(X_train,X_test,y_train,y_test,epochs,noOfNeuronsInMiddleLayer,noOfNeuronsInOutputLayer)
#predictions = getGRURNN2(X_train,X_test,y_train,y_test,epochs,noOfNeuronsInMiddleLayer,noOfNeuronsInOutputLayer)
confusionMatrix = np.empty([7, 7])

print("------------------------------")

predictions = binarizePredicitions(predictions)

multiclassConfusionMatrix(y_test,predictions)


np.set_printoptions(suppress=True)
print(multilabel_confusion_matrix(y_test,predictions))
print(precision_score(y_test,predictions,average="micro"))
print("--------------")
# Print the confusion matrix
print(metrics.multilabel_confusion_matrix(y_test, predictions))

# Print the precision and recall, among other metrics
print(metrics.classification_report(y_test, predictions, digits=3))
#print(confusionMatrix)
#print("Precision",calculateMicroPrecision(confusionMatrix))


# Convert the model.
converter = tf.lite.TFLiteConverter.from_keras_model(model)
tflite_model = converter.convert()

# Save the model.
with open('../gruModel2.tflite', 'wb') as f:
  f.write(tflite_model)

