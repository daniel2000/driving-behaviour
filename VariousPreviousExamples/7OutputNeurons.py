import os
import tempfile
import tensorflow as tf

from sklearn.metrics import multilabel_confusion_matrix

from RNN.gruRnn import *
import statistics

epochs = 1000
test_size = 0.3
noOfNeuronsInMiddleLayer = 10
noOfNeuronsInOutputLayer = 7


np.set_printoptions(suppress=True)

"""
# Obtaining the csv file having all outputs on 1 coloumn
X50LagAccOnly,y50lagAccOnly = returnXYData(8, 5)

X50LagAccOnly.to_pickle("X8lagAccOnly")
y50lagAccOnly.to_pickle("y8lagAccOnly")
"""

X= pd.read_pickle("InputPickleFiles/50LagAccOnly/X50LagAccOnly")
y= pd.read_pickle("InputPickleFiles/50LagAccOnly/y50lagAccOnly")

X_train, X_test, y_train, y_test = train_test_split(X, y, test_size=test_size)
NUMERIC_COLUMNS = list(X.columns)
test = X_train.iloc[0]
print(test)

file = open("test.txt","w")
for i in test:
    file.write(str(i))
    file.write("f,")

file.close()
#predictions, model = getGRURNNModelNEW(X_train,X_test,y_train,y_test,epochs,noOfNeuronsInMiddleLayer,noOfNeuronsInOutputLayer)
predictions,model = getGRURNNModel(X_train,X_test,y_train,y_test,epochs,noOfNeuronsInMiddleLayer,noOfNeuronsInOutputLayer)
#predictions = getGRURNN2(X_train,X_test,y_train,y_test,epochs,noOfNeuronsInMiddleLayer,noOfNeuronsInOutputLayer)
confusionMatrix = np.empty([7, 7])


for i in range(len(predictions)):
    maximum = max(predictions[i])
    for j in range(len(predictions[i])):
        if predictions[i][j] == maximum:
            predictions[i][j] = 1
        else:
            predictions[i][j] = 0



for i in range(len(predictions)):
    truthColNo = -1
    predColNo = -1
    found = False
    for j in range(len(predictions[0])):
        if predictions[i,j] == 1 and y_test.iat[i,j] == 1:
            confusionMatrix[j,j] += 1
            found = True
            break
        if predictions[i,j] == 1:
            predColNo = j
        if y_test.iat[i,j] == 1:
            truthColNo = j

    if not found:
        confusionMatrix[truthColNo,predColNo] += 1


np.set_printoptions(suppress=True)
print(multilabel_confusion_matrix(y_test,predictions))
print(precision_score(y_test,predictions,average="micro"))
print(confusionMatrix)
print("Precision",calculateMicroPrecision(confusionMatrix))

# Print the precision and recall, among other metrics
print(metrics.classification_report(y_test, predictions, digits=4))


# Convert the model.
converter = tf.lite.TFLiteConverter.from_keras_model(model)
tflite_model = converter.convert()

# Save the model.
with open('gruModel.tflite', 'wb') as f:
  f.write(tflite_model)

"""
feature_columns = []

for feature_name in NUMERIC_COLUMNS:
    feature_columns.append(tf.feature_column.numeric_column(feature_name,
                                                            dtype=tf.float32))



tmpdir = tempfile.mkdtemp()

serving_input_fn = tf.estimator.export.build_parsing_serving_input_receiver_fn(
  tf.feature_column.make_parse_example_spec(feature_columns))

estimator_base_path = os.path.join(tmpdir, 'from_estimator')
estimator_path = model.export_saved_model(estimator_base_path, serving_input_fn)


imported = tf.saved_model.load(estimator_path)
concrete_func = imported.signatures['serving_default']

Convert the model
converter = tf.lite.TFLiteConverter.from_concrete_functions([concrete_func])
converter.target_spec.supported_ops = [
  tf.lite.OpsSet.TFLITE_BUILTINS, # enable TensorFlow Lite ops.
  tf.lite.OpsSet.SELECT_TF_OPS # enable TensorFlow ops.
]
converter.allow_custom_ops = True
converter.experimental_new_converter =True

tflite_model = converter.convert()

Save the model.
with open('GRUmodel.tflite', 'wb') as f:
  f.write(tflite_model)

"""