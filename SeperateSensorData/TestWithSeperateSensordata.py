from RNN.gruRnn import *
from Helper.FormatAndObtainData import *


X,y = returnXYData(8,4)
epochs = 1000
noOfNeuronsInMiddleLayer = 10
noOfNeurosnInOutputLayer = 1

# Split dataset into training set and test set
X_train, X_test, y_train, y_test = train_test_split(X, y, test_size=0.3)


predicated, model = getGRURNNModel(X_train,X_test,y_train,y_test,epochs,noOfNeuronsInMiddleLayer,noOfNeurosnInOutputLayer)
testPredictInt = np.rint(predicated)
confMatrix = confusionMatrix(y_test,testPredictInt)
print("Precision", calculateMicroPrecision(confMatrix))