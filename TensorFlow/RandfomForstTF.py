## TL;DR:
# Train model.
# est = tf.estimator.BoostedTreesClassifier(feature_columns, n_batches_per_layer)
# est.train(train_input_fn)
# Per instance model interpretability:
# pred_dict = est.experimental_predict_with_explanations(pred_input_fn)
# Global gain-based feature importances:
# importances = est.experimental_feature_importances()

import tempfile
import os

import tensorflow as tf


# tf.enable_eager_execution()
print(tf.__version__)

# Load dataset.
from sklearn.model_selection import train_test_split

from Helper.FormatAndObtainData import returnXYData


# dftrain = pd.read_csv('https://storage.googleapis.com/tf-datasets/titanic/train.csv')
# dfeval = pd.read_csv('https://storage.googleapis.com/tf-datasets/titanic/eval.csv')
# y_train = dftrain.pop('survived')
# y_eval = dfeval.pop('survived')

X, y= returnXYData(8,2)

# Split dataset into training set and test set
X_train, X_test, y_train, y_test = train_test_split(X, y, test_size=0.3)

fc = tf.feature_column
# CATEGORICAL_COLUMNS = ['sex', 'n_siblings_spouses', 'parch', 'class', 'deck','embark_town', 'alone']
NUMERIC_COLUMNS = ['gyroscopeX_0', 'gyroscopeY_0', 'gyroscopeZ_0', 'magneticFieldX_0', 'magneticFieldY_0',
                   'magneticFieldZ_0', 'accelerometerX_0', 'accelerometerY_0', 'accelerometerZ_0',
                   'linearAccelerationX_0', 'linearAccelerationY_0', 'linearAccelerationZ_0', 'gyroscopeX_1'
    , 'gyroscopeY_1'
    , 'gyroscopeZ_1'
    , 'magneticFieldX_1'
    , 'magneticFieldY_1'
    , 'magneticFieldZ_1'
    , 'accelerometerX_1'
    , 'accelerometerY_1'
    , 'accelerometerZ_1'
    , 'linearAccelerationX_1'
    , 'linearAccelerationY_1'
    , 'linearAccelerationZ_1'
    , 'gyroscopeX_2'
    , 'gyroscopeY_2'
    , 'gyroscopeZ_2'
    , 'magneticFieldX_2'
    , 'magneticFieldY_2'
    , 'magneticFieldZ_2'
    , 'accelerometerX_2'
    , 'accelerometerY_2'
    , 'accelerometerZ_2'
    , 'linearAccelerationX_2'
    , 'linearAccelerationY_2'
    , 'linearAccelerationZ_2'
    , 'gyroscopeX_3'
    , 'gyroscopeY_3'
    , 'gyroscopeZ_3'
    , 'magneticFieldX_3'
    , 'magneticFieldY_3'
    , 'magneticFieldZ_3'
    , 'accelerometerX_3'
    , 'accelerometerY_3'
    , 'accelerometerZ_3'
    , 'linearAccelerationX_3'
    , 'linearAccelerationY_3'
    , 'linearAccelerationZ_3'
    , 'gyroscopeX_4'
    , 'gyroscopeY_4'
    , 'gyroscopeZ_4'
    , 'magneticFieldX_4'
    , 'magneticFieldY_4'
    , 'magneticFieldZ_4'
    , 'accelerometerX_4'
    , 'accelerometerY_4'
    , 'accelerometerZ_4'
    , 'linearAccelerationX_4'
    , 'linearAccelerationY_4'
    , 'linearAccelerationZ_4'
    , 'gyroscopeX_5'
    , 'gyroscopeY_5'
    , 'gyroscopeZ_5'
    , 'magneticFieldX_5'
    , 'magneticFieldY_5'
    , 'magneticFieldZ_5'
    , 'accelerometerX_5'
    , 'accelerometerY_5'
    , 'accelerometerZ_5'
    , 'linearAccelerationX_5'
    , 'linearAccelerationY_5'
    , 'linearAccelerationZ_5'
    , 'gyroscopeX_6'
    , 'gyroscopeY_6'
    , 'gyroscopeZ_6'
    , 'magneticFieldX_6'
    , 'magneticFieldY_6'
    , 'magneticFieldZ_6'
    , 'accelerometerX_6'
    , 'accelerometerY_6'
    , 'accelerometerZ_6'
    , 'linearAccelerationX_6'
    , 'linearAccelerationY_6'
    , 'linearAccelerationZ_6'
    , 'gyroscopeX_7'
    , 'gyroscopeY_7'
    , 'gyroscopeZ_7'
    , 'magneticFieldX_7'
    , 'magneticFieldY_7'
    , 'magneticFieldZ_7'
    , 'accelerometerX_7'
    , 'accelerometerY_7'
    , 'accelerometerZ_7'
    , 'linearAccelerationX_7'
    , 'linearAccelerationY_7'
    , 'linearAccelerationZ_7'
    , 'gyroscopeX_8'
    , 'gyroscopeY_8'
    , 'gyroscopeZ_8'
    , 'magneticFieldX_8'
    , 'magneticFieldY_8'
    , 'magneticFieldZ_8'
    , 'accelerometerX_8'
    , 'accelerometerY_8'
    , 'accelerometerZ_8'
    , 'linearAccelerationX_8'
    , 'linearAccelerationY_8'
    , 'linearAccelerationZ_8']


def one_hot_cat_column(feature_name, vocab):
    return tf.feature_column.indicator_column(
        tf.feature_column.categorical_column_with_vocabulary_list(feature_name,
                                                                  vocab))


feature_columns = []
"""
for feature_name in CATEGORICAL_COLUMNS:
    # Need to one-hot encode categorical features.
    vocabulary = dftrain[feature_name].unique()
    feature_columns.append(one_hot_cat_column(feature_name, vocabulary))
"""

for feature_name in NUMERIC_COLUMNS:
    feature_columns.append(tf.feature_column.numeric_column(feature_name,
                                                            dtype=tf.float32))

example = dict(X_train.head(1))

#class_fc = tf.feature_column.indicator_column(tf.feature_column.categorical_column_with_vocabulary_list('class', ('non-aggressive event', 'Second', 'Third')))
# print('Feature value: "{}"'.format(example['event'].iloc[0]))
# print('One-hot encoded: ', tf.keras.layers.DenseFeatures([class_fc])(example).numpy())

# Feature value: "Third"
# One-hot encoded:  [[0. 0. 1.]]


# Use entire batch since this is such a small dataset.
NUM_EXAMPLES = len(y_train)


def make_input_fn(X, y, n_epochs=None, shuffle=True):
    def input_fn():
        dataset = tf.data.Dataset.from_tensor_slices((dict(X), y))
        if shuffle:
            dataset = dataset.shuffle(NUM_EXAMPLES)
        # For training, cycle thru dataset as many times as need (n_epochs=None).
        dataset = dataset.repeat(n_epochs)
        # In memory training doesn't use batching.
        dataset = dataset.batch(NUM_EXAMPLES)
        return dataset

    return input_fn


# Training and evaluation input functions.
train_input_fn = make_input_fn(X_train, y_train)
eval_input_fn = make_input_fn(X_test, y_test, shuffle=False, n_epochs=1)

#linear_est = tf.estimator.LinearClassifier(feature_columns)

# Train model.
#linear_est.train(train_input_fn, max_steps=100)

# Evaluation.
#result = linear_est.evaluate(eval_input_fn)
#print("Linear Classifier", result)

# Since data fits into memory, use entire dataset per layer. It will be faster.
# Above one batch is defined as the entire dataset.
n_batches = 1
est = tf.estimator.BoostedTreesClassifier(feature_columns,
                                          n_batches_per_layer=n_batches)

# The model will stop training once the specified number of trees is built, not
# based on the number of steps.
est.train(train_input_fn, max_steps=100)


# Eval.
result = est.evaluate(eval_input_fn)
print("Boosted Tree", result)


tmpdir = tempfile.mkdtemp()

serving_input_fn = tf.estimator.export.build_parsing_serving_input_receiver_fn(
  tf.feature_column.make_parse_example_spec(feature_columns))

estimator_base_path = os.path.join(tmpdir, 'from_estimator')
estimator_path = est.export_saved_model(estimator_base_path, serving_input_fn)


imported = tf.saved_model.load(estimator_path)
concrete_func = imported.signatures['serving_default']

# Convert the model
converter = tf.lite.TFLiteConverter.from_concrete_functions([concrete_func])
converter.target_spec.supported_ops = [
  tf.lite.OpsSet.TFLITE_BUILTINS, # enable TensorFlow Lite ops.
  tf.lite.OpsSet.SELECT_TF_OPS # enable TensorFlow ops.
]
converter.allow_custom_ops = True
converter.experimental_new_converter =True

tflite_model = converter.convert()

# Save the model.
with open('model.tflite', 'wb') as f:
  f.write(tflite_model)



"""
serving_input_fn = tf.estimator.export.build_parsing_serving_input_receiver_fn(tf.feature_column.make_parse_example_spec(feature_columns))
tf.estimator.BoostedTreesClassifier.export_saved_model()



tmpdir = tempfile.mkdtemp()

mobilenet_save_path = os.path.join(tmpdir, "mobilenet/1/")
tf.saved_model.save(est, mobilenet_save_path)

loaded = tf.saved_model.load(mobilenet_save_path)
print(list(loaded.signatures.keys()))  # ["serving_default"]


infer = loaded.signatures["serving_default"]
print(infer.structured_outputs)
"""


"""
#tmpdir = tempfile.mkdtemp()


#estimator_base_path = os.path.join(tmpdir, 'from_estimator')
estimator_path = tf.estimator.Estimator.export_saved_model(est, "model322", serving_input_fn)

# Convert the model
converter = tf.lite.TFLiteConverter.from_saved_model("model322/1600954189",signature_keys="serving_default") # path to the SavedModel directory
tflite_model = converter.convert()

"""

"""
Save the model.
with open('modelOld.tflite', 'wb') as f:
  f.write(tflite_model)


tf.saved_model.save(est,"model2")

Convert the model
converter = tf.lite.TFLiteConverter.from_saved_model("model2") # path to the SavedModel directory
tflite_model = converter.convert()

 Save the model.
with open('modelOld.tflite', 'wb') as f:
  f.write(tflite_model)


feature_columns = [tf.feature_column.numeric_column(key=key) for key in X_train.keys()]
feature_spec = tf.feature_column.make_parse_example_spec(feature_columns)

Build receiver function, and export.
serving_input_receiver_fn = tf.estimator.export.build_parsing_serving_input_receiver_fn(feature_spec)

est.export_saved_model('model', serving_input_receiver_fn)

Convert the model
converter = tf.lite.TFLiteConverter.from_saved_model('model/1600950541  ') # path to the SavedModel directory
tflite_model = converter.convert()

Save the model.
with open('modelOld.tflite', 'wb') as f:
  f.write(tflite_model)
"""

