# https://www.datatechnotes.com/2018/12/rnn-example-with-keras-simplernn-in.html

from NewVersions.HelperFile import *
from Helper.ModelConverterToTFLITE import convertModelToTFLITE

import datetime
import sklearn
from keras.layers import GRU
from sklearn.model_selection import train_test_split
from tensorflow.python.keras.utils.vis_utils import plot_model
from tensorboard.plugins.hparams import api as hp
from keras.callbacks import LambdaCallback
from Helper.FormatAndObtainData import *
from keras.layers import Dense, Concatenate, Input
from keras.models import Model
from sklearn.model_selection import StratifiedKFold, KFold
from statistics import mean
from sklearn.preprocessing import OneHotEncoder


dateAndTimeNow = datetime.datetime.now().strftime("%Y%m%d-%H%M%S")

HP_NUM_UNITS = hp.HParam('num_units', hp.Discrete([7]))
HP_BATCH_SIZE = hp.HParam('batch_size', hp.Discrete([32]))
HP_ACT_FUNC = hp.HParam('activation_function', hp.Discrete(["softmax"]))
HP_OPTIMIZER = hp.HParam('optimizer', hp.Discrete(["rmsprop"]))

METRIC_ACCURACY = 'accuracy'

with tf.summary.create_file_writer('logs/hparam_tuning/' + dateAndTimeNow).as_default():
    hp.hparams_config(
        hparams=[HP_NUM_UNITS, HP_BATCH_SIZE, HP_ACT_FUNC],
        metrics=[hp.Metric(METRIC_ACCURACY, display_name='Accuracy')],
    )


def train_test_model(hparams):
    class_names = ["Non Aggressive Event", "Aggresive Right Turn", "Aggresive Left Turn", "Aggresive Acceleration",
                   "Aggresive Braking", "Aggresive Left Lane Change", "Aggresive Right Lane Change"]

    def log_confusion_matrix(epoch, logs):
        # Use the model to predict the values from the validation dataset.
        test_pred_raw = model.predict(X_test)
        test_pred = np.argmax(test_pred_raw, axis=1)
        y_test2 = np.argmax(y_test, axis=1)
        # Calculate the confusion matrix.
        cm = sklearn.metrics.confusion_matrix(y_test2, test_pred)
        # Log the confusion matrix as an image summary.
        figure = plot_confusion_matrix(cm, class_names=class_names)
        cm_image = plot_to_image(figure)

        # Log the confusion matrix as an image summary.
        with file_writer_cm.as_default():
            tf.summary.image("Confusion Matrix", cm_image, step=epoch)

    # Define the per-epoch callback.
    cm_callback = LambdaCallback(on_epoch_end=log_confusion_matrix)

    # Definition of Model
    input = Input(shape=(numofLag, 3))
    branch_outputs = []
    for i in range(10):
        # Slicing the ith channel:
        #out = Lambda(lambda x: x[:, i, :])(input)

        # Setting up your per-channel layers (replace with actual sub-models):
        out = GRU(units=7, activation="softmax")(input)
        branch_outputs.append(out)

    # Concatenating together the per-channel results:
    out = Concatenate()(branch_outputs)

    # Adding some further layers (replace or remove with your architecture):
    out = Dense(units=7, activation="softmax")(out)

    # Building model:
    model = Model(inputs=input, outputs=out)
    model.compile(loss='mean_squared_error', optimizer="rmsprop", metrics=['acc'])
    plot_model(model, to_file='model_plot' + dateAndTimeNow + '.png', show_shapes=True, show_layer_names=True)

    model.fit(X_train, y_train, epochs=epochs, batch_size=hparams[HP_BATCH_SIZE], callbacks=[
        tf.keras.callbacks.TensorBoard(
            "logs/fit/" + dateAndTimeNow), cm_callback])  # Run with 1 epoch to speed things up for demo purposes
    test_predictions = model.predict(X_test)

    y_test2 = np.argmax(y_test, axis=1)
    test_predictions2 = np.argmax(test_predictions, axis=1)
    print(sklearn.metrics.multilabel_confusion_matrix(y_test2, test_predictions2))

    _, accuracy = model.evaluate(X_test, y_test)

    convertModelToTFLITE(model, "ExactCopy4-Acc" + str(accuracy))
    return accuracy


def run(run_dir, hparams):
    with tf.summary.create_file_writer(run_dir).as_default():
        hp.hparams(hparams)  # record the values used in this trial
        accuracy = train_test_model(hparams)
        tf.summary.scalar(METRIC_ACCURACY, accuracy, step=1)
    return accuracy


session_num = 0
import pickle

if __name__ == '__main__':
    N = 1000
    Tp = 800
    epochs = 300
    noOfNeuronsInOutputLayer = 7

    accuracy_list = []

    # Obtaining the csv file having all outputs on 1 coloumn
    # X49LagAccData,y49lagAccData = returnXYData(49, 5)

    # X49LagAccData.to_pickle("X49LagAccData")
    # y49lagAccData.to_pickle("y49lagAccData")

    kf = KFold(n_splits=10)

    numofLag = 49 + 1
    X = pd.read_pickle("../InputPickleFiles/49LagLinearAccDataWithMyData/X49LinearAccDataWithMyData")
    y = pd.read_pickle("../InputPickleFiles/49LagLinearAccDataWithMyData/y49LinearAccDataWithMyData")

    X = X.to_numpy()
    y = y.to_numpy()

    y = revert_onehot_encoding(y)
    counter = 0

    for train_index, test_index in kf.split(X, y):
        print("TRAIN:", train_index, "TEST:", test_index)
        X_train, X_test = X[train_index], X[test_index]
        y_train, y_test = y[train_index], y[test_index]

        pickle.dump(X_test, open("KFoldTest/X-test" + str(counter) + ".pickle", "wb"))
        pickle.dump(y_test, open("KFoldTest/y-test" + str(counter) + ".pickle", "wb"))
        counter+=1

        y_train = convert_to_one_hot_encoding(y_train)
        y_test = convert_to_one_hot_encoding(y_test)
        # X_train, X_test, y_train, y_test = train_test_split(X, y, test_size=0.3)

        # X_train = np.reshape(X_train, (X_train.shape[0], X_train.shape[1], 1))
        # X_test = np.reshape(X_test, (X_test.shape[0], X_test.shape[1], 1))
        X_train = convertToIndividualArray(X_train, numofLag)
        X_test = convertToIndividualArray(X_test, numofLag)

        logdir = "logs/image/" + dateAndTimeNow
        # Define the basic TensorBoard callback.
        tensorboard_callback = tf.keras.callbacks.TensorBoard(log_dir=logdir)
        file_writer_cm = tf.summary.create_file_writer(logdir + '/cm')


        for num_units in HP_NUM_UNITS.domain.values:
            for batch_size in HP_BATCH_SIZE.domain.values:
                for act_func in HP_ACT_FUNC.domain.values:
                    for optimizer in HP_OPTIMIZER.domain.values:
                        hparams = {
                            HP_NUM_UNITS: num_units,
                            HP_BATCH_SIZE: batch_size,
                            HP_ACT_FUNC: act_func,
                            HP_OPTIMIZER: optimizer,
                        }
                        run_name = "run-%d" % session_num
                        print('--- Starting trial: %s' % run_name)
                        print({h.name: hparams[h] for h in hparams})
                        accuracy_list.append(run('logs/hparam_tuning/' + dateAndTimeNow + run_name, hparams))
                        session_num += 1

        print(accuracy_list)
        print(mean(accuracy_list))


