import pickle
import pandas as pd
import numpy as np
print("Train Test Split")
df = pickle.load( open( "X-text.pickle", "rb" ) )
df = pickle.load( open( "y-test.pickle", "rb" ) )

histogram= [0,0,0,0,0,0,0]

df = df.to_numpy()

for row in df:
    for c,col in enumerate(row):
        if col == 1:
            histogram[c]+=1
            break

print(histogram)
print("Total " + str(len(df)))

print("Normal K Fold")
df = pickle.load( open( "KFoldTest/X-test0.pickle", "rb" ) )
df = pickle.load( open( "KFoldTest/y-test0.pickle", "rb" ) )

histogram= [0,0,0,0,0,0,0]


for row in df:
    histogram[row]+=1


print(histogram)
print("Total " + str(len(df)))

print("Stratified")
df = pickle.load( open( "KFoldTestStratified/X-test0.pickle", "rb" ) )
df = pickle.load( open( "KFoldTestStratified/y-test0.pickle", "rb" ) )

histogram= [0,0,0,0,0,0,0]


for row in df:
    histogram[row]+=1


print(histogram)
print("Total " + str(len(df)))


"""
CopyOfModel - 3 stratified k fold
Acc = 0.63

ExactCopyofModel4 - Normal 30% test set
Acc = 0.99


CopyOfModelKFold - 10 normal kfold
Acc = 0.81


"""

