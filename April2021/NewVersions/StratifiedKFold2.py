# https://www.datatechnotes.com/2018/12/rnn-example-with-keras-simplernn-in.html



from Helper.FormatAndObtainData import *
from HelperFile import *
from sklearn.model_selection import train_test_split
import datetime
import pickle
import os
from sklearn.model_selection import StratifiedKFold


if __name__ == '__main__':
    main_dir_name = "CSVFiles/KFoldStratified" + str(datetime.datetime.now().strftime("%m.%d.%Y, %H.%M.%S"))
    os.mkdir(main_dir_name)

    #X = pd.read_pickle("../../InputPickleFiles/49LagLinearAccData/X49LagLinearAccData")
    #y = pd.read_pickle("../../InputPickleFiles/49LagLinearAccData/y49lagLinearAccData")
    X = pd.read_pickle("XExampleData.pickle")
    y = pd.read_pickle("yExampleData.pickle")

    X = X.to_numpy()
    y = y.to_numpy()

    counter = 0

    skf = StratifiedKFold(n_splits=5,shuffle=True)

    y = revert_onehot_encoding(y)

    for train_index, test_index in skf.split(X, y):
        total_dir = main_dir_name + "/Run_" + str(counter)
        os.mkdir(total_dir)
        counter+=1
        epochs = 1
        numofLag = 49 + 1


        X_train, X_test = X[train_index], X[test_index]
        y_train, y_test = y[train_index], y[test_index]

        y_train = convert_to_one_hot_encoding(y_train)
        y_test = convert_to_one_hot_encoding(y_test)

        X_train = convertToIndividualArray(X_train, numofLag)
        X_test = convertToIndividualArray(X_test, numofLag)

        save_data(total_dir, X_train, X_test, y_train, y_test)

        y_test, y_pred,y_test_argmax, y_pred_argmax = train_test_model(X_train,y_train,X_test,y_test,numofLag,epochs)

        save_to_csv_file(total_dir,y_train, y_test, y_pred, y_test_argmax, y_pred_argmax)







