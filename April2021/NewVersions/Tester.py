import pickle
import pandas as pd
import numpy as np

"""
dirname = "CSVFiles/TrainTestSplit04.14.2021, 12.13.51/"
with open(dirname + 'X_train.pickle', 'rb') as handle:
    X_train = pickle.load(handle)
with open(dirname + 'y_train.pickle', 'rb') as handle:
    y_train = pickle.load(handle)
with open(dirname + 'X_test.pickle', 'rb') as handle:
    X_test = pickle.load(handle)
with open(dirname + 'y_test.pickle', 'rb') as handle:
    y_test = pickle.load(handle)
    
"""

def save_pickle(filename, data):
    export = open(filename, 'wb')
    pickle.dump(data, export)
    export.close()

# Distribution
# 50,70,80,88,93,97,100
initial_x = np.zeros((100,150))
initial_y = np.zeros((100,7))

for i in range(initial_x.shape[0]):
    for j in range(initial_x.shape[1]):
        if i <= 50:
            initial_x[i,j] = 0
            initial_y[i,0] = 1
        if i > 50 and i <= 70:
            initial_x[i,j] = 1
            initial_y[i,1] = 1
        if i > 70 and i <= 80:
            initial_x[i, j] = 2
            initial_y[i,2] = 1
        if i > 80 and i <= 88:
            initial_x[i, j] = 3
            initial_y[i,3] = 1
        if i > 88 and i <= 93:
            initial_x[i, j] = 4
            initial_y[i,4] = 1
        if i > 93 and i <= 97:
            initial_x[i, j] = 5
            initial_y[i, 5] = 1
        if i > 97 and i <= 100:
            initial_x[i, j] = 6
            initial_y[i,6] = 1



X = pd.DataFrame(initial_x)
y = pd.DataFrame(initial_y)

save_pickle("XExampleData.pickle",X)
save_pickle("yExampleData.pickle",y)

print("X")