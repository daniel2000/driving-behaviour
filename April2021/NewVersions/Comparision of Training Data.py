
import pickle
from HelperFile import *

run_no =0

X_UAH = pickle.load( open( "C:\\Users\\attar\\PycharmProjects\\DrivingBehaviour\\ExactCopyOfModel\\X_UAH_9", "rb" ) )
y_UAH = pickle.load( open( "C:\\Users\\attar\\PycharmProjects\\DrivingBehaviour\\ExactCopyOfModel\\Y_UAH_9", "rb" ) )


X_train_normal_new2 = pickle.load(open("CSVFiles/Old/TrainTestSplit04.19.2021, 11.58.13/X_train.pickle", "rb"))
X_train_normal_new = pickle.load(open("CSVFiles/Old/TrainTestSplit04.19.2021, 11.57.49/X_train.pickle", "rb"))

X_train_normal = pickle.load( open( "CSVFiles/LinearData/TrainTestSplit04.15.2021, 07.52.27/X_train.pickle", "rb" ) )
X_test_normal = pickle.load( open( "CSVFiles/LinearData/TrainTestSplit04.15.2021, 07.52.27/X_test.pickle", "rb" ) )
y_train_normal = pickle.load( open( "CSVFiles/LinearData/TrainTestSplit04.15.2021, 07.52.27/y_train.pickle", "rb" ) )
y_test_normal = pickle.load( open( "CSVFiles/LinearData/TrainTestSplit04.15.2021, 07.52.27/y_test.pickle", "rb" ) )


X_train_stratified = pickle.load( open( "CSVFiles/LinearData/TrainTestSplitStratified04.15.2021, 07.52.16/X_train.pickle", "rb" ) )
X_test_stratified = pickle.load( open( "CSVFiles/LinearData/TrainTestSplitStratified04.15.2021, 07.52.16/X_test.pickle", "rb" ) )
y_train_stratified = pickle.load( open( "CSVFiles/LinearData/TrainTestSplitStratified04.15.2021, 07.52.16/y_train.pickle", "rb" ) )
y_test_stratified = pickle.load( open( "CSVFiles/LinearData/TrainTestSplitStratified04.15.2021, 07.52.16/y_test.pickle", "rb" ) )

X_train_kfold =pickle.load( open( "CSVFiles/LinearData/KFoldStratified04.15.2021, 07.51.56/Run_" +str(run_no)+"/X_train.pickle", "rb" ) )
X_test_kfold = pickle.load( open( "CSVFiles/LinearData/KFoldStratified04.15.2021, 07.51.56/Run_"+ str(run_no)+"/X_test.pickle", "rb" ) )
y_train_kfold = pickle.load( open( "CSVFiles/LinearData/KFoldStratified04.15.2021, 07.51.56/Run_"+ str(run_no)+"/y_train.pickle", "rb" ) )
y_test_kfold = pickle.load( open( "CSVFiles/LinearData/KFoldStratified04.15.2021, 07.51.56/Run_"+ str(run_no)+"/y_test.pickle", "rb" ) )

X_train_kfold_shuffle = pickle.load( open( "CSVFiles/LinearData/KFoldStratified04.20.2021, 17.15.00Shuffle=True/Run_" + str(run_no)+ "/X_train.pickle", "rb"))
X_test_kfold_shuffle = pickle.load( open( "CSVFiles/LinearData/KFoldStratified04.20.2021, 17.15.00Shuffle=True/Run_" + str(run_no)+ "/X_test.pickle", "rb"))
y_train_kfold_shuffle = pickle.load( open( "CSVFiles/LinearData/KFoldStratified04.20.2021, 17.15.00Shuffle=True/Run_" + str(run_no)+ "/y_train.pickle", "rb"))
y_test_kfold_shuffle = pickle.load( open( "CSVFiles/LinearData/KFoldStratified04.20.2021, 17.15.00Shuffle=True/Run_" + str(run_no)+ "/y_test.pickle", "rb"))



X_train_stratified = convertToIndividualArray(X_train_stratified, 50)
X_test_stratified = convertToIndividualArray(X_test_stratified, 50)

def get_distribution(array):
    histogram = [0, 0, 0, 0, 0, 0, 0]
    for row in array:
        for c,col in enumerate(row):
            if col == 1:
                histogram[c]+=1
                break

    total = np.sum(histogram)
    percentage_distribution = [0, 0, 0, 0, 0, 0, 0]
    for i in range(len(histogram)):
        percentage_distribution[i] = "{:.2f}".format(histogram[i] / total)

    print(histogram)
    print(percentage_distribution)


def compare_two_datasets(array_one, array_two):
    equal_rows = 0
    total_rows = array_one.shape[0]
    for row_one in range(array_one.shape[0]):
        for row_two in range(array_two.shape[0]):
            if array_one[row_one,0,0] != array_two[row_two,0,0] or array_one[row_one,0,1] != array_two[row_two,0,1] or array_one[row_one,0,2] != array_two[row_two,0,2]:
                #print("gdf")
                continue
            if array_one[row_one, 49, 0] != array_two[row_two, 49, 0] or array_one[row_one, 49, 1] != array_two[
                row_two, 49, 1] or array_one[row_one, 49, 2] != array_two[row_two, 49, 2]:
                continue

            # If arrived here, the first and last xyz values are equal
            equal_rows +=1
            break

    return equal_rows,total_rows


equal_rows,total_rows = compare_two_datasets(X_train_normal,X_test_normal)
print(equal_rows)
print(total_rows)

print("Y_train Normal")
#get_distribution(y_train_normal)
print("Y_train Stratified")
#get_distribution(y_train_stratified)
print("Y_train k Fold")
#get_distribution(y_train_kfold)
print("Y_train k Fold Shuffle")
#get_distribution(y_train_kfold_shuffle)
print("UAH")
get_distribution(y_UAH.to_numpy())
print("------------")
print("Y_test")
print("------------")
print("Y_test Normal")
#get_distribution(y_test_normal)
print("Y_test Stratified")
#get_distribution(y_test_stratified)
print("Y_test k Fold")
#get_distribution(y_test_kfold)

print("Y_test k Fold Shuffle")
#get_distribution(y_test_kfold_shuffle)



