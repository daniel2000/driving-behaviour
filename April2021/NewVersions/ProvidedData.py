# https://www.datatechnotes.com/2018/12/rnn-example-with-keras-simplernn-in.html


import sklearn
from statistics import mean
from sklearn.preprocessing import OneHotEncoder

from Helper.FormatAndObtainData import *
from sklearn.model_selection import StratifiedKFold

from HelperFile import *
from sklearn.model_selection import train_test_split
import datetime

import os

if __name__ == '__main__':
    dir_name = "CSVFiles/ProvidedData" + str(datetime.datetime.now().strftime("%m.%d.%Y, %H.%M.%S"))
    os.mkdir(dir_name)
    epochs = 350
    numofLag = 49 + 1

    accuracy_list = []

    X_train = pd.read_pickle("CSVFiles/LinearData/KFoldStratified04.15.2021, 07.51.56/Run_0/X_train.pickle")
    X_test = pd.read_pickle("CSVFiles/LinearData/KFoldStratified04.15.2021, 07.51.56/Run_0/X_test.pickle")
    y_train = pd.read_pickle("CSVFiles/LinearData/KFoldStratified04.15.2021, 07.51.56/Run_0/y_train.pickle")
    y_test = pd.read_pickle("CSVFiles/LinearData/KFoldStratified04.15.2021, 07.51.56/Run_0/y_test.pickle")



    save_data(dir_name, X_train, X_test, y_train, y_test)
    y_test, y_pred,y_test_argmax, y_pred_argmax = train_test_model(X_train,y_train,X_test,y_test,numofLag,epochs)

    save_to_csv_file(dir_name,y_test, y_pred,y_test_argmax, y_pred_argmax)







