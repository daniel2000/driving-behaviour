# https://www.datatechnotes.com/2018/12/rnn-example-with-keras-simplernn-in.html



from Helper.FormatAndObtainData import *
from HelperFile import *
from sklearn.model_selection import train_test_split
import datetime
import pickle
import os

if __name__ == '__main__':
    dir_name = "CSVFiles/TrainTestSplitStratified" + str(datetime.datetime.now().strftime("%m.%d.%Y, %H.%M.%S"))
    os.mkdir(dir_name)
    epochs = 500
    numofLag = 49 + 1

    accuracy_list = []

    #X = pd.read_pickle("../../InputPickleFiles/49LagLinearAccData/X49LagLinearAccData")
    #y = pd.read_pickle("../../InputPickleFiles/49LagLinearAccData/y49lagLinearAccData")

    X = pd.read_pickle("XExampleData.pickle")
    y = pd.read_pickle("yExampleData.pickle")

    X = X.to_numpy()
    y = y.to_numpy()

    counter = 0

    X_train, X_test, y_train, y_test = train_test_split(X, y, test_size = 0.2,stratify=y)



    X_train = convertToIndividualArray(X_train, numofLag)
    X_test = convertToIndividualArray(X_test, numofLag)

    save_data(dir_name, X_train, X_test, y_train, y_test)

    y_test, y_pred,y_test_argmax, y_pred_argmax = train_test_model(X_train,y_train,X_test,y_test,numofLag,epochs)

    save_to_csv_file(dir_name,y_train, y_test, y_pred,y_test_argmax, y_pred_argmax)







