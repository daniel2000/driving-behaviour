import csv
import pickle
import datetime

import numpy as np
import pandas as pd
import matplotlib.pyplot as plt

run_no =4
from HelperFile import *



X_train_normal = pickle.load( open( "CSVFiles/LinearData/TrainTestSplit04.15.2021, 07.52.27/X_train.pickle", "rb" ) )
X_test_normal = pickle.load( open( "CSVFiles/LinearData/TrainTestSplit04.15.2021, 07.52.27/X_test.pickle", "rb" ) )
y_train_normal = pickle.load( open( "CSVFiles/LinearData/TrainTestSplit04.15.2021, 07.52.27/y_train.pickle", "rb" ) )
y_test_normal = pickle.load( open( "CSVFiles/LinearData/TrainTestSplit04.15.2021, 07.52.27/y_test.pickle", "rb" ) )


X_train_stratified = pickle.load( open("CSVFiles/LinearData/TrainTestSplitStratified04.15.2021, 07.52.16/X_train.pickle", "rb" ) )
X_test_stratified = pickle.load( open("CSVFiles/LinearData/TrainTestSplitStratified04.15.2021, 07.52.16/X_test.pickle", "rb" ) )
y_train_stratified = pickle.load( open("CSVFiles/LinearData/TrainTestSplitStratified04.15.2021, 07.52.16/y_train.pickle", "rb" ) )
y_test_stratified = pickle.load( open( "CSVFiles/LinearData/TrainTestSplitStratified04.15.2021, 07.52.16/y_test.pickle", "rb" ) )
X_train_stratified = convertToIndividualArray(X_train_stratified, 50)
X_test_stratified = convertToIndividualArray(X_test_stratified, 50)

X_train_kfold = pickle.load( open( "CSVFiles/LinearData/KFoldStratified04.15.2021, 07.51.56/Run_" + str(run_no)+ "/X_train.pickle", "rb"))
X_test_kfold = pickle.load( open( "CSVFiles/LinearData/KFoldStratified04.15.2021, 07.51.56/Run_" + str(run_no)+ "/X_test.pickle", "rb"))
y_train_kfold = pickle.load( open( "CSVFiles/LinearData/KFoldStratified04.15.2021, 07.51.56/Run_" + str(run_no)+ "/y_train.pickle", "rb"))
y_test_kfold = pickle.load( open( "CSVFiles/LinearData/KFoldStratified04.15.2021, 07.51.56/Run_" + str(run_no)+ "/y_test.pickle", "rb"))

X_train_kfold_shuffle = pickle.load( open( "CSVFiles/LinearData/KFoldStratified04.20.2021, 17.15.00Shuffle=True/Run_" + str(run_no)+ "/X_train.pickle", "rb"))
X_test_kfold_shuffle = pickle.load( open( "CSVFiles/LinearData/KFoldStratified04.20.2021, 17.15.00Shuffle=True/Run_" + str(run_no)+ "/X_test.pickle", "rb"))
y_train_kfold_shuffle = pickle.load( open( "CSVFiles/LinearData/KFoldStratified04.20.2021, 17.15.00Shuffle=True/Run_" + str(run_no)+ "/y_train.pickle", "rb"))
y_test_kfold_shuffle = pickle.load( open( "CSVFiles/LinearData/KFoldStratified04.20.2021, 17.15.00Shuffle=True/Run_" + str(run_no)+ "/y_test.pickle", "rb"))


X = pd.read_pickle("../../InputPickleFiles/49LagLinearAccData/X49LagLinearAccData")
y = pd.read_pickle("../../InputPickleFiles/49LagLinearAccData/y49lagLinearAccData")


def obtain_similarity(original_array, new_array):
    distribution = []
    counter = 0
    for orig_array_row in original_array:
        counter += 1
        for row in new_array:

            if row[ 0, 0] != orig_array_row[0] or row[ 0, 1] != orig_array_row[1] or row[ 0, 2] != orig_array_row[2]:
                continue
            if row[49, 0] != orig_array_row[147] or row[49, 1] != orig_array_row[148] or row[49, 2] != orig_array_row[149]:
                continue


            # If arrived here, the first and last xyz values are equal
            distribution.append(counter-1)

            break
    print(len(distribution))
    return distribution


def compare_two_datasets(array_one, array_two):
    equal_rows = 0
    total_rows = array_one.shape[0]
    for row_one in range(array_one.shape[0]):
        for row_two in range(array_two.shape[0]):
            if array_one[row_one,0] != array_two[row_two,0,0] or array_one[row_one,1] != array_two[row_two,0,1] or array_one[row_one,2] != array_two[row_two,0,2]:
                #print("gdf")
                continue
            if array_one[row_one, 147] != array_two[row_two, 49, 0] or array_one[row_one,148] != array_two[
                row_two, 49, 1] or array_one[row_one, 149] != array_two[row_two, 49, 2]:
                continue

            # If arrived here, the first and last xyz values are equal
            equal_rows +=1
            break

    return equal_rows,total_rows

def output_histogram_data(original_array,filename, histogram_data):
    with open('Histogram' + filename +'.csv', mode='w') as employee_file:
        employee_writer = csv.writer(employee_file, delimiter=',', quotechar='"', quoting=csv.QUOTE_MINIMAL)

        employee_writer.writerow(["Histogram of train events"])
        employee_writer.writerow(histogram_data)

def show_histogram(histogram_name, data):
    import matplotlib.pyplot as plt


    plt.title(histogram_name)
    plt.hist(data, bins=7966,rwidth=1)
    plt.show()

#equal_rows, total_rows = compare_two_datasets(X.to_numpy(),X_train_normal)
histogram_of_data_train = obtain_similarity(X.to_numpy(),X_train_stratified)
histogram_of_data_test = obtain_similarity(X.to_numpy(),X_test_stratified)
#output_histogram_data(X,"KFold1"+ str(datetime.datetime.now().strftime("%m.%d.%Y, %H.%M.%S")),histogram_of_data)
show_histogram("X Train Stratified",histogram_of_data_train)
show_histogram("X Test Stratified",histogram_of_data_test)



