model = Sequential()
    model.add(Conv1D(input_shape=(step, 3),filters=hparams[HP_NUM_UNITS], kernel_size=1, activation=hparams[HP_ACT_FUNC]))
    model.add(Flatten())
    model.add(Dense(noOfNeuronsInOutputLayer, activation=hparams[HP_ACT_FUNC]))
    plot_model(model, to_file='model_plot' + dateAndTimeNow + '.png', show_shapes=True, show_layer_names=True)

    model.compile(loss='mean_squared_error', optimizer=hparams[HP_OPTIMIZER], metrics=['acc'])
    model.summary()


    model.fit(X_train, y_train, epochs=epochs,batch_size=hparams[HP_BATCH_SIZE],callbacks=[tf.keras.callbacks.TensorBoard("logs/fit/" + dateAndTimeNow)]) # Run with 1 epoch to speed things up for demo purposes
    _, accuracy = model.evaluate(X_test, y_test)

    return accuracy